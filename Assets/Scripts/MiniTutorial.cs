﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniTutorial : MonoBehaviour {

	void Start ()
    {
        this.gameObject.SetActive(true);
        InvokeRepeating("AmIFinished", 1, 1);
	}

    void AmIFinished() //Cuando finaliza las animaciones
    {
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("MiniTutorial"))
        {
            if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !GetComponent<Animator>().IsInTransition(0))
            {
                this.gameObject.SetActive(false);
                CancelInvoke();
            }
        }
    }
}
