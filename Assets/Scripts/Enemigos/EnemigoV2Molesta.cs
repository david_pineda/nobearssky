﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemigoV2Molesta : MonoBehaviour {


    public NavMeshAgent agent;

    public GameObject target;
    public GameObject[] puntos;
    //public Transform target;
    public int moveSpeed = 2;
    private int rotationSpeed = 6;
    public bool patrulla, shock;
    public int ladistancia = 15;
    private Transform puntoActual;
    public bool grabPlayer;
    public GameObject m_Camera;
    void Awake()
    {
        //myTransform = transform;
        agent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        patrulla = false; shock = grabPlayer = false;
        puntos = GameObject.FindGameObjectsWithTag("Point");
        target = GameObject.FindGameObjectWithTag("Player");
        m_Camera = GameObject.FindGameObjectWithTag("MainCamera");
        //target = GameObject.FindWithTag("Player").transform; //target the player
    }

    public void shocked()
    {
        setpuntoAMover();
        shock = true;
        m_Camera.GetComponent<ComportamientoCamara>().activada = true;
        grabPlayer = false;
        target.GetComponent<MovimientoPersonaje>().Continuar();


    }

    public void Raptar()
    {
       // m_Camera.GetComponent<ComportamientoCamara>().activada = false;
        grabPlayer = true;
        setpuntoAMover();
       // target.GetComponent<MovimientoPersonaje>().Detener();
        //  target.transform.SetParent(this.transform, true);
    }

    void OnCollisionEnter(Collision otherObj)
    {
        if (otherObj.gameObject.tag == "Player")
            if (!shock)
            {
                {
                    print("te tengo ");
                    Raptar();


                }
            }
    }

    void setpuntoAMover()
    {
        int numero = (int)Random.Range(0.0f, puntos.Length);
        agent.SetDestination(puntos[numero].transform.position);
        print(numero);
        puntoActual = puntos[numero].transform;
        patrulla = true;
    }

    void Update()
    {

        if (Input.GetKey("l"))
        {
            m_Camera.GetComponent<ComportamientoCamara>().activada = false;
        }


        float distancia;
        //Calcular distancia
        distancia = Vector3.Distance(target.transform.position, transform.position);

        //Si la distancia es menor a 4
        if ((distancia < ladistancia) && (!shock) && (!grabPlayer))
        {
            patrulla = false;
            //Voltear
            agent.SetDestination(target.transform.position);
            //Lineas de debug que aparecen en la ventana Scene
            Debug.DrawLine(target.transform.position, transform.position, Color.red, Time.deltaTime, false);
        }
        else
        {
            if (puntos.Length > 0)
            {
                if (!patrulla)
                {
                    setpuntoAMover();
                }
                float DPunto = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(puntoActual.position.x, puntoActual.position.z));
                //  if ((transform.position.x==puntoActual.position.x)&&(transform.position.z == puntoActual.position.z)) {
                if (DPunto < 2)
                {
                    patrulla = false;
                    shock = false;
                    print("llege");
                }
            }
        }

        if (grabPlayer)
        {
            agent.SetDestination(this.transform.position);
            if (distancia > ladistancia/2){
                grabPlayer = false;
            }

        }

    }
}

