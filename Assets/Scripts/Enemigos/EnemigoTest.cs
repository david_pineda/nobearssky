﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoTest : MonoBehaviour {

    public Transform target;
    public int moveSpeed = 2;
    private int rotationSpeed = 6;

    public int ladistancia = 15;
    private Transform myTransform;

    void Awake()
    {
        myTransform = transform;
    }

    void Start()
    {
        target = GameObject.FindWithTag("Player").transform; //target the player
    }

    void Update()
    {
        float distancia;
        //Calcular distancia
        distancia = Vector3.Distance(target.position, transform.position);

        //Si la distancia es menor a 4
        if (distancia < ladistancia)
        {
            //Voltear
            myTransform.rotation = Quaternion.Slerp(myTransform.rotation,
            Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed * Time.deltaTime);
            //Caminar
            myTransform.position += myTransform.forward * moveSpeed * Time.deltaTime;
            //Lineas de debug que aparecen en la ventana Scene
            Debug.DrawLine(target.transform.position, transform.position, Color.red, Time.deltaTime, false);
        }
    }
}
