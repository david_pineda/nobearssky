﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class Enemigo_test2 : MonoBehaviour
{
    
   public  NavMeshAgent agent;

    GameObject target;

    void Awake()
    {

        agent = GetComponent<NavMeshAgent>();

        target = GameObject.FindGameObjectWithTag("Player");

       agent.SetDestination(target.transform.position);

    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(target.transform.position);
    }
}