﻿public var target : Transform; 
var moveSpeed = 3;
private var rotationSpeed = 6;

public var ladistancia= 15;
var myTransform : Transform;


function Awake()
{
    myTransform = transform;
}


function Start(){
    target = GameObject.FindWithTag("Player").transform; //target the player

}


function Update () {
    //Calcular distancia
    var distancia : float;
    distancia = Vector3.Distance(target.transform.position, transform.position);

    //Si la distancia es menor a 4
    if(distancia<ladistancia){
        //Voltear
        myTransform.rotation = Quaternion.Slerp(myTransform.rotation,
	    Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed*Time.deltaTime);
        //Caminar
        myTransform.position += myTransform.forward * moveSpeed * Time.deltaTime;
        //Lineas de debug que aparecen en la ventana Scene
        Debug.DrawLine (target.transform.position, transform.position, Color.red,  Time.deltaTime, false);
    }
}