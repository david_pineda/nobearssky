﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoCamara : MonoBehaviour {

    public GameObject player;
    public float damping = 2;
    Vector3 desiredPosition;
    public bool activada;
    private Rigidbody rb;
    private Vector3 offset;

    // para el screenshake 

    float shakeAmount  = 0.7f;
    bool shake;

    void Start()
    {
        try
        {
            player = GameObject.FindWithTag("Player");
        }
        catch
        {
            Debug.Log("Poner Jugador en player");
        }

        try
        {
            rb = GetComponent<Rigidbody>();
            SetRigidbody();
        }
        catch
        {
            Debug.Log("Le hace falta RigidBody a la camara");
        }
        activada = true;
        offset = transform.position - player.transform.position;
        shake = false;

        Reinicio.EnReiniciando += Reiniciar;
    }

    void FixedUpdate()
    {


        if (activada)
        {
            desiredPosition = player.transform.position + offset;
            Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
            rb.MovePosition(position);
        }
       
    }

    void SetRigidbody()
    {
        rb.isKinematic = true;
        rb.constraints = RigidbodyConstraints.FreezePositionY;
    }

    private void Update()
    {
        if (shake)
        {
            Vector2 ShakePos = Random.insideUnitCircle * shakeAmount * (Time.deltaTime * 60);
            transform.position = new Vector3(transform.position.x+ShakePos.x, transform.position.y ,transform.position.z + ShakePos.y);
        }
    }

    public void Shakecamera(bool theShake)
    {
       
            shake = theShake;
       
    }

    public void Reiniciar()
    {
        desiredPosition = player.transform.position + offset;
        transform.position = desiredPosition;
    }

}
