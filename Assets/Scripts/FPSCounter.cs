﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCounter : MonoBehaviour {

    UILord ui;

    double fps;
    float frameCount = 0;
    float dt = 0;
    float updateRate = 4;
	// Use this for initialization
	void Start () {
        ui = UILord.Instancia;
        fps = 1;
	}
	
	// Update is called once per frame
	void Update ()
    {
        frameCount++;
        dt += Time.deltaTime;

        if (dt > 1.0f/updateRate)
        {
            fps = frameCount / dt;
            frameCount = 0;
            dt -= 1.0f / updateRate;
            ui.fpsText.text = "FPS: " + fps.ToString();
        }

	}
}
