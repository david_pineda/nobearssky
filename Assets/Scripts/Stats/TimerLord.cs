﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerLord : MonoBehaviour {

    /// <summary>
    /// Este Script Controla los timers generales de los Stats
    /// </summary>
    /// 


    public delegate void GameOver();
    public static event GameOver EnPerdida;

    public float[] maxStat;
    private int totalStats;

    public bool gameOver;

    public float[] speed;

    float[] timersStats;
    float timer;

    public int seMejora;
    IndicadorDiaNoche indicador;

    private UILord ui;
    private ResourceManager manager;
    private MisionesLord misiones;

    void Awake()
    {
        ui = UILord.Instancia;

        if (ui.contador == null)
        {
            ui.BuscarAgain();
        }
    }

    void Start ()
    {
        TimeWarpLord.EnPasoDelTiempo += TimeWarpStats; //recibe el evento de NaveTrigger y lo envía al metodo TimeWarpStats

        seMejora = 0;
        indicador = IndicadorDiaNoche.Instancia;
        misiones = MisionesLord.Instancia;

        totalStats = 3;
        maxStat = new float[totalStats];
        maxStat[0] = 12.5f; //Cuanto es el valor maximo del stat // 5 dias
        maxStat[1] = 120; // 3 dias
        maxStat[2] = 8.4f; // 7 dias

        manager = GameObject.FindWithTag("GameController").GetComponent<ResourceManager>();

        ui.Buscar(totalStats);

        timersStats = new float[totalStats];
        speed = new float[totalStats];

        timer = 0;

        gameOver = false;

        for (int i = 0; i < totalStats; i++ )
        {
            ui.menuStatSlider[i].maxValue = ui.sliderUI[i].maxValue = maxStat[i];
            ui.menuStatSlider[i].value = ui.sliderUI[i].value = maxStat[i];
            timersStats[i] = maxStat[i];
        }

        //Cada dia debe gastarse esto de recurso
        speed[0] = 2.5f; //Hambre
        speed[1] = 40f; //Calor
        speed[2] = 1.20f; //Oxigeno

        SetInicio();

        SetTextoMenu();
        SetSlider();

        Reinicio.EnReiniciando += Reiniciar;
    }

    void SetInicio()
    {
        timersStats[0] = speed[0] * 2;
        timersStats[1] = speed[1] * 1;
        timersStats[2] = speed[2] * 6;
    }

	// Update is called once per frame
	void FixedUpdate ()
    {
        if (gameOver == false)
        {
            timer += Time.fixedDeltaTime;


            if (timer >= 5)
            {
                SetTimers();
                timer = 0;
            }


            if (timersStats[0] <= 0 || timersStats[1] <= 0 || timersStats[2] <= 0 && gameOver == false)
            {
                EnviarEvento();
                gameOver = true;
            }
        }
	}

    void SetTimers()
    {
        for (int i = 0; i < totalStats; i++)
        {
            timersStats[i] -= ((timer*speed[i])/180);
            //timersStats[i] -= timer * 2;
            SetSlider();
            SetTextoMenu();
        } 
    }

    void SetSlider()
    {
        for (int i = 0; i < totalStats; i++)
        {
            ui.menuStatSlider[i].value = ui.sliderUI[i].value = timersStats[i];
        }
    }


    void SetTextoMenu() //Indica cuantos recursos se necesitan para los stats en Texto
    {
        ui.menuStatText[0].text = "1";
        ui.menuStatText[1].text = "2";
        ui.menuStatText[2].text = "1";

        if (timersStats[2] < maxStat[2])
        {
            ui.botonesMenu[4].interactable = true;
        }

        if (timersStats[0] < maxStat[0]) //Todos estos if revisan cuantos dias quedan para que se acabe el recurso
        {
            ui.botonesMenu[3].interactable = true;
        }
        if (timersStats[1] < maxStat[1])
        {
            ui.botonesMenu[5].interactable = true;
        }

        SetearMisiones();
            /*
            if (timersStats[0] < maxStat[0]) //Todos estos if revisan cuantos dias quedan para que se acabe el recurso
            {
                ui.botonesMenu[3].interactable = true;
                if (timersStats[0] > speed[0] * 4)
                {
                    ui.menuStatText[0].text = "1";
                }
                else if (timersStats[0] > speed[0] * 3)
                {
                    ui.menuStatText[0].text = "2";
                }
                else if (timersStats[0] > speed[0] * 2)
                {
                    ui.menuStatText[0].text = "3";
                }
                else if (timersStats[0] > speed[0] * 1)
                {
                    ui.menuStatText[0].text = "4";
                }
                else if (timersStats[0] > 0)
                {
                    ui.menuStatText[0].text = "5";
                }
                else
                {
                    ui.menuStatText[0].text = "X";
                }
            }
            else if (timersStats[0] >= maxStat[0])
            {
                ui.menuStatText[0].text = "0";
            }


            if (timersStats[1] < maxStat[1])
            {
                ui.botonesMenu[5].interactable = true;
                if (timersStats[1] > speed[1] * 2)
                {
                    ui.menuStatText[1].text = "2";
                }
                else if (timersStats[1] > speed[1] * 1)
                {
                    ui.menuStatText[1].text = "4";
                }
                else if (timersStats[1] > 0)
                {
                    ui.menuStatText[1].text = "6";
                }
                else
                {
                    ui.menuStatText[1].text = "X";
                }
            }
            else if (timersStats[1] >= maxStat[1])
            {
                ui.menuStatText[1].text = "0";
            }

            if (timersStats[2] < maxStat[2])
            {
                ui.botonesMenu[4].interactable = true;
                if (timersStats[2] > speed[2] * 6)
                {
                    ui.menuStatText[2].text = "1";
                }
                else if (timersStats[2] > speed[2] * 5)
                {
                    ui.menuStatText[2].text = "2";
                }
                else if (timersStats[2] > speed[2] * 4)
                {
                    ui.menuStatText[2].text = "3";
                }
                else if (timersStats[2] > speed[2] * 3)
                {
                    ui.menuStatText[2].text = "4";
                }
                else if (timersStats[2] > speed[2] * 2)
                {
                    ui.menuStatText[2].text = "5";
                }
                else if (timersStats[2] > speed[2] * 1)
                {
                    ui.menuStatText[2].text = "6";
                }
                else if (timersStats[2] > 0)
                {
                    ui.menuStatText[2].text = "7";
                }
                else
                {
                    ui.menuStatText[2].text = "X";
                }
            }
            else if (timersStats[2] >= maxStat[2])
            {
                ui.menuStatText[2].text = "0";
            }
            */
            //Aqui se acaban los if. Podría haber usado un metodo más eficiente, but fuck it
    }


    void SetearMisiones() //Esto está para cuadrar qué misión aparece en qué momento. Usualmente, la segunda misión
    {
        if (timersStats[1] <= speed[1] * 1)
        {
            misiones.CambiarMisionRecurso("Madera");
        }
        else if (timersStats[0] <= speed[0] * 1)
        {
            misiones.CambiarMisionRecurso("Miel");
        }
        else if (timersStats[2] <= speed[2] * 1)
        {
            misiones.CambiarMisionRecurso("Hielo");
        }
        else
        {
            misiones.CambiarMisionRecurso("");
        }

    }






    void EnviarEvento()
    {
        EnPerdida();
    }

    public void RecuperarStats(int statASubir) //Lo usan los botones para subir los stats
    {
        if (RevisarRecursos(statASubir) && RevisarNecesidad(statASubir))
        {
            switch (statASubir)
            {
                case 1:
                    //hambre
                    manager.recurso1Contador -= 1;
                    timersStats[0] += 2.5f;
                    MaximizarStats(0);
                    ui.sliderUI[0].value = timersStats[0];
                    ui.menuStatSlider[0].value = timersStats[0];
                    break;

                case 2:
                    //Calor
                    manager.recurso2Contador -= 2;
                    timersStats[1] += 40f;
                    MaximizarStats(1);
                    ui.sliderUI[1].value = timersStats[1];
                    ui.menuStatSlider[1].value = timersStats[1];
                    break;

                case 3:
                    //Oxígeno
                    manager.recurso3Contador -= 1;
                    timersStats[2] += 1f;
                    MaximizarStats(2);
                    ui.sliderUI[2].value = timersStats[2];
                    ui.menuStatSlider[2].value = timersStats[2];
                    break;

                default:
                    break;
            }
            manager.UpdateTexto();
            SetTextoMenu();
        }
        else if (!RevisarRecursos(statASubir))
        {
            ui.algoSucede.text = "Faltan Recursos";
        }
        else
        {
            ui.algoSucede.text = "Al Máximo";
        }
    }

    private bool RevisarRecursos(int statRevisado)
    {
        switch (statRevisado)
        {
            case 1:
                if (manager.recurso1Contador >= 1)
                {
                    return true;
                }
                break;

            case 2:
                if (manager.recurso2Contador >= 2)
                {
                    return true;
                }
                break;

            case 3:
                if (manager.recurso3Contador >= 1)
                {
                    return true;
                }
                break;

            default:
                break;
        }

        return false;
    }

    bool RevisarNecesidad(int statRevisado)
    {
        if (timersStats[statRevisado - 1] >= maxStat[statRevisado - 1])
        {
            return false; //no se necesita
        }
        else
        {
            return true; //Se necesita subir el stat
        }
    }

    void MaximizarStats(int stat) //Evita que se salgan de rango los stats
    {
        if (timersStats[stat] >= maxStat[stat])
        {
            timersStats[stat] = maxStat[stat];

            switch(stat)
            {
                case 0:
                    ui.botonesMenu[3].interactable = false; //Hambre
                    break;

                case 1:
                    ui.botonesMenu[5].interactable = false;  //Calor
                    break;

                case 2:
                    ui.botonesMenu[4].interactable = false;  //Oxigeno
                    break;

                default:
                    break;
            }
        }
    }

    public void DecidirAlUpgradear(int _i)
    {
        if (indicador.dia == false)
        {
            seMejora = _i;
        }
    }

    void TimeWarpStats() //Gasta 1 dia de recursos al ocurrir el timewarp
    {
        if (timersStats[0] < maxStat[0] && seMejora != 1) //Todos estos if revisan si el timerStats de cada stat es mayor a x dias de uso
        {
            seMejora = 0;
            if (timersStats[0] >= speed[0] * 4)
            {
                timersStats[0] = speed[0] * 4;
            }
            else if (timersStats[0] >= speed[0] * 3)
            {
                timersStats[0] = speed[0] * 3;
            }
            else if (timersStats[0] >= speed[0] * 2)
            {
                timersStats[0] = speed[0] * 2;
            }
            else if (timersStats[0] >= speed[0] * 1)
            {
                timersStats[0] = speed[0] * 1;
            }
            else if (timersStats[0] > 0)
            {
                timersStats[0] = 0;
            }
            else
            {
                timersStats[0] = -500;
            }
        }
        else if (timersStats[0] >= maxStat[0])
        {
            timersStats[0] = maxStat[0];
        }


        if (timersStats[1] < maxStat[1] && seMejora != 2)
        {
            seMejora = 0;
            if (timersStats[1] >= speed[1] * 2)
            {
                timersStats[1] = speed[1] * 2;
            }
            else if (timersStats[1] >= speed[1] * 1)
            {
                timersStats[1] = speed[1] * 1;
            }
            else if (timersStats[1] > 0)
            {
                timersStats[1] = 0;
            }
            else
            {
                timersStats[1] = -500;
            }
        }
        else if (timersStats[1] >= maxStat[1])
        {
            timersStats[1] = maxStat[1];
        }

        if (timersStats[2] < maxStat[2] && seMejora != 3)
        {
            seMejora = 0;
            if (timersStats[2] >= speed[2] * 6)
            {
                timersStats[2] = speed[2] * 6;
            }
            else if (timersStats[2] >= speed[2] * 5)
            {
                timersStats[2] = speed[2] * 5;
            }
            else if (timersStats[2] >= speed[2] * 4)
            {
                timersStats[2] = speed[2] * 4;
            }
            else if (timersStats[2] >= speed[2] * 3)
            {
                timersStats[2] = speed[2] * 3;
            }
            else if (timersStats[2] >= speed[2] * 2)
            {
                timersStats[2] = speed[2] * 2;
            }
            else if (timersStats[2] >= speed[2] * 1)
            {
                timersStats[2] = speed[2] * 1;
            }
            else if (timersStats[2] > 0)
            {
                timersStats[2] = 0;
            }
            else
            {
                timersStats[2] = -500;
            }
        }
        else if (timersStats[2] >= maxStat[2])
        {
            timersStats[2] = maxStat[2];
        }
        //Aqui terminan los if

        SetTextoMenu();
        SetSlider();
    }


    //Reiniciar

    void Reiniciar()
    {
        seMejora = 0;

        maxStat[0] = 12.5f; //Cuanto es el valor maximo del stat // 5 dias
        maxStat[1] = 120; // 3 dias
        maxStat[2] = 8.4f; // 7 dias

        timer = 0;

        gameOver = false;

        for (int i = 0; i < totalStats; i++)
        {
            ui.menuStatSlider[i].maxValue = ui.sliderUI[i].maxValue = maxStat[i];
            ui.menuStatSlider[i].value = ui.sliderUI[i].value = maxStat[i];
            timersStats[i] = maxStat[i];
        }

        //Cada dia debe gastarse esto de recurso
        speed[0] = 2.5f; //Hambre
        speed[1] = 40f; //Calor
        speed[2] = 1.20f; //Oxigeno

        SetInicio();

        SetTextoMenu();
        SetSlider();
    }
}
