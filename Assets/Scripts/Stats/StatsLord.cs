﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsLord {


    public static StatsLord instancia = null;

    private float[] CantidadTotal;
    private float[] CantidadActual;
    private float[] Necesidades;


    public static StatsLord Instancia
    {
        get
        {
            if (instancia == null)
            {
                instancia = new StatsLord();
            }
            return (instancia);
        }
    }

    private StatsLord()
    {
        Necesidades = new float[4];
        CantidadTotal = new float[4];
        CantidadActual = new float[4];

        CantidadTotal[0] = 0;
        CantidadTotal[1] = 100; //en segundos/minutos yo que se
        CantidadTotal[2] = 100;
        CantidadTotal[3] = 100;

        for (int i = 0; i < Necesidades.Length; i++)
        {
            Necesidades[i] = 0;
            CantidadActual[i] = 0;
        }
    }
}
