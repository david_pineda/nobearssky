﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuLord : MonoBehaviour {

    private UILord ui;
    private PausaLord pausa;

    private UpgradeManager upgrade;

    public bool estaEnMenu;

    private AudioSource sonar;
    public AudioClip menuEntrarSonido, menuSalirSonido, menuBoton, menuMaquina;

	void Start () {
        upgrade = UpgradeManager.Instancia;

        ui = UILord.Instancia;
        pausa = PausaLord.Instancia;

        estaEnMenu = false;
        sonar = this.GetComponent<AudioSource>();

        NaveTrigger.EnMenu += MenuAparecer;
        NaveTrigger.EnSalida += MenuDesaparecer;
	}
	
	void Update ()
    {
        /*
		if (Input.GetKeyUp(KeyCode.Q))
        {
            if (estaEnMenu == false)
            {
                MenuAparecer();
            }
            else
            {
                MenuDesaparecer();
            }
        }
        */
    }

    void MenuAparecer()
    {
        if (!pausa.noPausar)
        {
            estaEnMenu = true;
            pausa.Pausar();
            ui.ActivarMenu();

            if (sonar == null)
            {
                sonar = this.GetComponent<AudioSource>();
            }
            sonar.clip = menuEntrarSonido;
            sonar.Play();
        }
    }

    void MenuDesaparecer()
    {
        if (!pausa.noPausar)
        {
            estaEnMenu = false;
            pausa.Pausar();
            ui.DesactivarMenu();

            if (sonar == null)
            {
                sonar = this.GetComponent<AudioSource>();
            }
            sonar.clip = menuSalirSonido;
            sonar.Play();
        }
    }

    public void SonidoBoton()
    {
        sonar.clip = menuBoton;
        sonar.Play();
    }

    public void SonidoMaquina()
    {
        if (upgrade.sePuedeMejorar)
        {
            sonar.clip = menuMaquina;
            sonar.Play();
        }
    }

}
