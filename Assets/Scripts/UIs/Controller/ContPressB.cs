﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ContPressB : MonoBehaviour {

    EventSystem eve;
    Button butt;

    void Start()
    {
        eve = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        butt = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.JoystickButton1))
        {
            //eve.SetSelectedGameObject(butt.gameObject);
            butt.onClick.Invoke();
        }
    }
}
