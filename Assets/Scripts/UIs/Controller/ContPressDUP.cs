﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ContPressDUP : MonoBehaviour {

    EventSystem eve;
    Button butt;
    int waiting;

    void Start()
    {
        waiting = 0;

        eve = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        butt = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("VerticalD") >= 1 && waiting == 0)
        {
            waiting = 1;
        }

        if (waiting == 1 && Input.GetAxis("VerticalD") == 0)
        {
            butt.onClick.Invoke();
            waiting = 0;
        }
    }
}
