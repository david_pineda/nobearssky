﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DesaparecerTexto : MonoBehaviour {

    Color alpha;
    Text yo;
    string nada;
    string anterior;

    // Use this for initialization
    void Start()
    {
        nada = "";
        yo = GetComponent<Text>();
        anterior = yo.text;
        alpha = yo.color;
        alpha.a = 1;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (yo.color.a > 0 && yo.text != "")
        {
            alpha.a -= 0.008f;
            yo.color = alpha;

            if (alpha.a <= 0)
            {
                alpha.a = 1;
                yo.text = nada;
                yo.color = alpha;
            }
        }
        if (yo.text == "")
        {
            alpha.a = 1;
            yo.color = alpha;
        }
        if (yo.text != anterior)
        {
            anterior = yo.text;
            alpha.a = 1;
            yo.color = alpha;
        }

        anterior = yo.text;
    }
}
