﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlechaApuntar : MonoBehaviour {

    Vector3 tmpVector;
    GameObject player;
    Transform nave;
    float angleToTarget;

    UILord ui;

	void Start () {
        ui = UILord.Instancia;
        player = GameObject.FindWithTag("Player");
        nave = GameObject.FindWithTag("Nave").GetComponent<Transform>();
	}
	
	void Update () {

        tmpVector = player.transform.position - nave.position;
        tmpVector.Normalize();
        angleToTarget = Mathf.Atan2(tmpVector.z, tmpVector.x) * Mathf.Rad2Deg;
        ui.flecha.gameObject.transform.localEulerAngles = new Vector3(0, 0, angleToTarget);
    }
    
}
