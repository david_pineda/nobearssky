﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ControllerSupport : MonoBehaviour {

    EventSystem eve;
    Button butt;

    bool onlyOnce;

	// Use this for initialization
	void Start () {
        onlyOnce = false;
        eve = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        butt = GetComponent<Button>();
	}
	
	

    public void Select()
    {
        eve.SetSelectedGameObject(butt.gameObject);
    }
}
