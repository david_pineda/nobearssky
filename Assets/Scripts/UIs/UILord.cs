﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILord {

    public Text recurso;
    public Slider sliderRecoleccion;

    public Text contador;
    public Text contador2;
    public Text contador3;

    public Slider[] sliderUI;

    public Text gameOver;

    public Text pausa;


    public Image panelMenu;

    public Text recurso1;
    public Text recurso2;
    public Text recurso3;

    public Text[] menuStatText;

    public Text[] menuRecurso;

    public Image flecha;

    public Slider energia;
    public Image fillEnergia;
    public Color fillColor;

    public Image black;

    public Text indicaciones;
    public Text escudoText;
    public Text dias;

    public Text presionar;
    public Text invalido;

    public Slider[] menuStatSlider;

    public Button[] botonesMenu;

    public Text[] mision;

    private GameObject[] usadosGameplay; //Lista de GameObjects que tengan el tag "DuringGameplay"
    private GameObject[] usadosMenu1; //Lista de GameObjects con tag Menu

    public Text fpsText;

    public Text algoSucede;

    public static UILord instancia = null;

    public static UILord Instancia
    {
        get
        {
            if (instancia == null)
            {
                instancia = new UILord();
            }
            return (instancia);
        }
    }

    public UILord()
    {
        if (contador == null)
        {
            indicaciones = GameObject.Find("IndicacionesText").GetComponent<Text>();
            escudoText = GameObject.Find("EscudoText").GetComponent<Text>();
            dias = GameObject.Find("DiasTexto").GetComponent<Text>();
            algoSucede = GameObject.Find("AlgoSucedioText").GetComponent<Text>();

            presionar = GameObject.Find("PresionarText").GetComponent<Text>();
            invalido = GameObject.Find("InvalidoText").GetComponent<Text>();


            botonesMenu = new Button[6];
            menuRecurso = new Text[3];
            menuStatText = new Text[3];
            menuStatSlider = new Slider[3];
            mision = new Text[3];
            for (int i = 0; i < menuRecurso.Length; i++)
            {
                menuStatText[i] = GameObject.Find("statText (" + i + ")").GetComponent<Text>();
                menuRecurso[i] = GameObject.Find("ContadorText (" + i + ")").GetComponent<Text>();
                menuStatSlider[i] = GameObject.Find("SliderStat (" + i + ")").GetComponent<Slider>();
                mision[i] = GameObject.Find("MisionText (" + i + ")").GetComponent<Text>();
            }
            for (int i = 0; i < botonesMenu.Length; i++)
            {
                botonesMenu[i] = GameObject.Find("BotonMenu (" + i + ")").GetComponent<Button>();
            }

            flecha = GameObject.Find("imagenFlecha").GetComponent<Image>();

            black = GameObject.Find("BlacknessPanel").GetComponent<Image>();

            fpsText = GameObject.Find("FPS").GetComponent<Text>();


            ////Prueba para detectar todos los objetos con tag
            usadosGameplay = GameObject.FindGameObjectsWithTag("DuringGameplay"); //Encuentra todos los gameobjects con el tag
            usadosMenu1 = GameObject.FindGameObjectsWithTag("Menu");

            sliderRecoleccion = GameObject.Find("SliderRecursos").GetComponent<Slider>();
            recurso = GameObject.Find("RecursoText").GetComponent<Text>();

            sliderRecoleccion.gameObject.SetActive(false);


            contador = GameObject.Find("ContadorText").GetComponent<Text>();
            contador2 = GameObject.Find("ContadorText2").GetComponent<Text>();
            contador3 = GameObject.Find("ContadorText3").GetComponent<Text>();

            gameOver = GameObject.Find("GameOverText").GetComponent<Text>();
            gameOver.text = "";

            pausa = GameObject.Find("PausaText").GetComponent<Text>();
            pausa.text = "";


            recurso1 = GameObject.Find("recurso1Text").GetComponent<Text>();
            recurso2 = GameObject.Find("recurso2Text").GetComponent<Text>();
            recurso3 = GameObject.Find("recurso3Text").GetComponent<Text>();

            energia = GameObject.Find("SliderEnergia").GetComponent<Slider>();
            fillEnergia = GameObject.FindWithTag("Especial").GetComponent<Image>();
            fillColor = fillEnergia.color;

            panelMenu = GameObject.Find("MenuPanel").GetComponent<Image>();
            panelMenu.gameObject.SetActive(false);

            EvitarCosas();
        }
    }
	
    public void BuscarAgain()
    {
        indicaciones = GameObject.Find("IndicacionesText").GetComponent<Text>();
        escudoText = GameObject.Find("EscudoText").GetComponent<Text>();
        dias = GameObject.Find("DiasTexto").GetComponent<Text>();

        presionar = GameObject.Find("PresionarText").GetComponent<Text>();
        invalido = GameObject.Find("InvalidoText").GetComponent<Text>();

        botonesMenu = new Button[6];
        menuRecurso = new Text[3];
        menuStatText = new Text[3];
        menuStatSlider = new Slider[3];
        mision = new Text[3];
        for (int i = 0; i < menuRecurso.Length; i++)
        {
            menuStatText[i] = GameObject.Find("statText (" + i + ")").GetComponent<Text>();
            menuRecurso[i] = GameObject.Find("ContadorText (" + i + ")").GetComponent<Text>();
            menuStatSlider[i] = GameObject.Find("SliderStat (" + i + ")").GetComponent<Slider>();
            mision[i] = GameObject.Find("MisionText (" + i + ")").GetComponent<Text>();
        }
        for (int i = 0; i < botonesMenu.Length; i++)
        {
            botonesMenu[i] = GameObject.Find("BotonMenu (" + i + ")").GetComponent<Button>();
        }

        flecha = GameObject.Find("imagenFlecha").GetComponent<Image>();

        black = GameObject.Find("BlacknessPanel").GetComponent<Image>();

        fpsText = GameObject.Find("FPS").GetComponent<Text>();


        ////Prueba para detectar todos los objetos con tag
        usadosGameplay = GameObject.FindGameObjectsWithTag("DuringGameplay"); //Encuentra todos los gameobjects con el tag
        usadosMenu1 = GameObject.FindGameObjectsWithTag("Menu");

        sliderRecoleccion = GameObject.Find("SliderRecursos").GetComponent<Slider>();
        recurso = GameObject.Find("RecursoText").GetComponent<Text>();

        sliderRecoleccion.gameObject.SetActive(false);


        contador = GameObject.Find("ContadorText").GetComponent<Text>();
        contador2 = GameObject.Find("ContadorText2").GetComponent<Text>();
        contador3 = GameObject.Find("ContadorText3").GetComponent<Text>();

        gameOver = GameObject.Find("GameOverText").GetComponent<Text>();
        gameOver.text = "";

        pausa = GameObject.Find("PausaText").GetComponent<Text>();
        pausa.text = "";


        recurso1 = GameObject.Find("recurso1Text").GetComponent<Text>();
        recurso2 = GameObject.Find("recurso2Text").GetComponent<Text>();
        recurso3 = GameObject.Find("recurso3Text").GetComponent<Text>();

        energia = GameObject.Find("SliderEnergia").GetComponent<Slider>();
        fillEnergia = GameObject.FindWithTag("Especial").GetComponent<Image>();
        fillColor = fillEnergia.color;

        panelMenu = GameObject.Find("MenuPanel").GetComponent<Image>();
        panelMenu.gameObject.SetActive(false);

        EvitarCosas();
    }

    public void Buscar(int CantidadDeStats)
    {
        sliderUI = new Slider[CantidadDeStats];

        for (int i = 0; i < CantidadDeStats; i++)
        {
            sliderUI[i] = GameObject.Find("SliderTest (" + i + ")").GetComponent<Slider>();
            sliderUI[i].interactable = false;
        }
    }

    private void EvitarCosas()
    {
        sliderRecoleccion.interactable = false;

        botonesMenu[3].interactable = false;
        botonesMenu[5].interactable = false;
        botonesMenu[4].interactable = false;

        invalido.text = "";
        presionar.text = "";
        algoSucede.text = "";
    }

    public void GameOverLayout()
    {
        sliderRecoleccion.gameObject.SetActive(false);
    }

    public void PausaTexto()
    {
        if (pausa.text == "" & gameOver.text == "")
        {
            pausa.text = "PAUSA";
        }
        else
        {
            pausa.text = "";
        }
    }

    public void ActivarMenu()
    {
        
        panelMenu.gameObject.SetActive(true);
        MenuRecursos(true);
        HUD(false);

    }

    public void DesactivarMenu()
    {
        algoSucede.text = "";
        panelMenu.gameObject.SetActive(false);
        MenuRecursos(false);
        HUD(true);
    }

    void HUD(bool activado)
    {
        for (int i = 0; i < usadosGameplay.Length; i++)
        {
            usadosGameplay[i].SetActive(activado);
        }
    }

    void MenuRecursos(bool activado)
    {
        for (int i = 0; i < usadosGameplay.Length; i++)
        {
            usadosMenu1[i].SetActive(activado);
        }
    }

    public void Reiniciar()
    {
        EvitarCosas();
        DesactivarMenu();
        pausa.text = "";
        gameOver.text = "";
        dias.text = "";
    }

}
