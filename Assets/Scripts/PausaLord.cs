﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class PausaLord {

    //La tecla para pausar esta controlada por MovimientoPersonaje

    public int setPausa;

    private UILord ui;

    public bool noPausar;

    public static PausaLord instancia = null;

    public static PausaLord Instancia
    {
        get
        {
            if (instancia == null)
            {
                instancia = new PausaLord();
            }
            return (instancia);
        }
    }

    private PausaLord()
    {
        setPausa = 0;
        ui = UILord.Instancia;
        noPausar = false;
    }
    

    public void Pausar()
    {
        if (!noPausar)
        {
            if (setPausa == 0)
            {
                Time.timeScale = 0;
                //AudioListener.pause = true;
                setPausa = 1;

                ui.PausaTexto();
            }

            else if (setPausa == 1)
            {
                Time.timeScale = 1;
                //AudioListener.pause = false;
                setPausa = 0;

                ui.PausaTexto();
            }
        }
    }
}
