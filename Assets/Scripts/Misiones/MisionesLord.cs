﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisionesLord {

    UILord ui;

    public static MisionesLord instancia = null;

    public static MisionesLord Instancia
    {
        get
        {
            if (instancia == null)
            {
                instancia = new MisionesLord();
            }
            return (instancia);
        }
    }

    public MisionesLord()
    {
        ui = UILord.Instancia;

        ui.mision[0].text = "Mision Principal";
        CambiarMisionPrincipal();

        ui.mision[1].text = "Mision Recursos más necesario";
        CambiarMisionRecurso("");


        ui.mision[2].text = "Mision conseguir x recursos";
        CambiarMisionExtra("Miel","Energía");

    }

    public void CambiarMisionPrincipal()
    {
        ui.mision[0].text = "Misión Principal: Buscar ayuda de la colonia cercana";
    }

    public void CambiarMisionRecurso(string recurso)
    {
        if (recurso == "")
        {
            ui.mision[1].text = "";
        }
        else
        {
            ui.mision[1].text = "Los oseznos necesitan " + recurso + "!";
        }
    } 

    public void CambiarMisionExtra(string recurso, string upgrade)
    {
        if (recurso == "" || upgrade == "")
        {
            ui.mision[2].text = "";
        }
        else
        {
            ui.mision[2].text = "Conseguir " + recurso + " para mejorar " + upgrade;
        }
    }

    public void Reinicio()
    {
        ui.mision[0].text = "Mision Principal";
        CambiarMisionPrincipal();

        ui.mision[1].text = "Mision Recursos más necesario";
        CambiarMisionRecurso("");


        ui.mision[2].text = "Mision conseguir x recursos";
        CambiarMisionExtra("Miel", "Energía");
    }
}
