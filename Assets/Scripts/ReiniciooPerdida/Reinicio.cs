﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reinicio : MonoBehaviour {

    PausaLord pausa;
    UILord ui;
    IndicadorDiaNoche indicador;
    MisionesLord misiones;
    UpgradeManager upgrade;
    //Recursos, ResourceManager, EscudoLord, player, tutorialLord, timeWarpLord. TimerLord, gameOverLord

    GameObject[] recursos;

    public delegate void Reiniciando();
    public static event Reiniciando EnReiniciando;
    public static event Reiniciando EnReinicioYa;

    void Awake()
    {
        recursos = GameObject.FindGameObjectsWithTag("Recurso");
    }

    void Start()
    {
        pausa = PausaLord.Instancia;
        ui = UILord.Instancia;
        indicador = IndicadorDiaNoche.Instancia;
        misiones = MisionesLord.Instancia;
        upgrade = UpgradeManager.Instancia;

        GameOverLord.EnGameOver += Reiniciar;
	}
	
	void Reiniciar()
    {
        //SceneManager.LoadScene("MenuPrincipal");

        pausa.setPausa = 1;
        pausa.Pausar();

        ui.Reiniciar();

        indicador.Reiniciar();

        misiones.Reinicio();

        upgrade.Reiniciar();

        EnReiniciando();

        for (int i = 0; i < recursos.Length; i++)
        {
            recursos[i].SetActive(true);
        }

        EnReinicioYa();
        ui.PausaTexto();
    }
}
