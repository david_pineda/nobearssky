﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perdida {

    public bool accidente = false; //cuando se acaba la energía o cuando se pierde contra el yeti/enemigo que se lo lleva
    public bool volverANormalidad = false;

    public bool reinicio = false;

    public static Perdida instancia = null;

    public static Perdida Instancia
    {
        get
        {
            if (instancia == null)
            {
                instancia = new Perdida();
            }
            return (instancia);
        }
    }

    public Perdida()
    {
    }
}

