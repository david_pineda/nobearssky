﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverLord : MonoBehaviour {

    private UILord ui;
    private MovimientoPersonaje movimiento;
    private ResourceManager recursos;
    private TimerLord stats;
    private PausaLord pausa;
    private MenuLord menu;

    public bool gameOver;

    public delegate void Final();
    public static event Final EnGameOver;

    void Start()
    {
        ui = UILord.Instancia;
        movimiento = GameObject.FindWithTag("Player").GetComponent<MovimientoPersonaje>();
        recursos = GameObject.FindWithTag("GameController").GetComponent<ResourceManager>();
        stats = GameObject.FindWithTag("GameController").GetComponent<TimerLord>(); 
        menu = GameObject.Find("MenuController").GetComponent<MenuLord>();
        pausa = PausaLord.Instancia;

        gameOver = false;

        TimerLord.EnPerdida += OsosMuertos;
        CivilizacionFinal.EnFinal += Rescate;
        Reinicio.EnReiniciando += Reiniciar;
	}

    void OsosMuertos()
    {
        ui.GameOverLayout();
        ui.gameOver.text = "Los osos han muerto...";

        movimiento.parar = true;
        pausa.Pausar();

        recursos.enabled = false;
        stats.enabled = false;

        gameOver = true;
    }

    void LateUpdate()
    {
        if ((Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.JoystickButton6)) && gameOver == true)
        {
            EnGameOver();
        }

        if ((pausa.setPausa == 1 && menu.estaEnMenu == false) && ((Input.GetKey(KeyCode.R) && Input.GetKey(KeyCode.T)) || (Input.GetKey(KeyCode.JoystickButton6) && Input.GetKey(KeyCode.JoystickButton0))))
        {
            EnGameOver();
        }

        if (pausa.setPausa == 0)
        {
            ui.pausa.text = "";
        }
    }

    void Rescate()
    {
        ui.GameOverLayout();
        ui.gameOver.text = "Rescate en Camino!";

        movimiento.parar = true;
        pausa.Pausar();

        recursos.enabled = false;
        stats.enabled = false;

        gameOver = true;
    }

    void Reiniciar()
    {
        ui.gameOver.text = "";

        movimiento.parar = false;

        recursos.enabled = true;
        stats.enabled = true;

        gameOver = false;
    }
}
