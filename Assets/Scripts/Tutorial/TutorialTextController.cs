﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TutorialTextController {

    public Image tutorial;

    public GameObject[] pagina;

    public Button adelante, atras;

    public EventSystem eve;

    public static TutorialTextController instancia = null;

    public static TutorialTextController Instancia
    {
        get
        {
            if (instancia == null)
            {
                instancia = new TutorialTextController();
            }
            return (instancia);
        }
    }

    public TutorialTextController()
    {
        tutorial = GameObject.FindWithTag("Tutorial").GetComponent<Image>();

        pagina = new GameObject[7];
        pagina[0] = GameObject.Find("IntroduccionTut");
        pagina[1] = GameObject.Find("MisionPrincipalTut");
        pagina[2] = GameObject.Find("NecesidadesTut");
        pagina[3] = GameObject.Find("MielTut");
        pagina[4] = GameObject.Find("MaderaTut");
        pagina[5] = GameObject.Find("HieloTut");
        pagina[6] = GameObject.Find("FinalTut");

        adelante = GameObject.Find("TutAdelanteBoton").GetComponent<Button>();
        atras = GameObject.Find("TutAtrasBoton").GetComponent<Button>();

        eve = GameObject.Find("EventSystem").GetComponent<EventSystem>();
    }

    public void DesactivarTutorial()
    {
        adelante.gameObject.SetActive(false);
        atras.gameObject.SetActive(false);
        for (int i = 0; i < pagina.Length; i++)
        {
            pagina[i].SetActive(false);
        }

        tutorial.gameObject.SetActive(false);
    }


}
