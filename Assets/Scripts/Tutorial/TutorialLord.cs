﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialLord : MonoBehaviour {

    TutorialTextController tut;
    PausaLord pausa;

    int ii;

    void Start()
    {
        tut = TutorialTextController.Instancia;
        pausa = PausaLord.Instancia;
        ii = 0;

        tut.adelante.onClick.AddListener(PasarPagina);
        tut.atras.onClick.AddListener(AtrasPagina);


        IniciarTutorial();

        Reinicio.EnReinicioYa += Reiniciar;
    }
	

    public void IniciarTutorial()
    {
        pausa.Pausar();
        pausa.noPausar = true;

        tut.DesactivarTutorial();
        ii = 0;

        tut.tutorial.gameObject.SetActive(true);
        tut.pagina[0].SetActive(true);
        tut.adelante.gameObject.SetActive(true);
        tut.eve.SetSelectedGameObject(tut.adelante.gameObject);
    }

    public void PasarPagina()
    {

        tut.pagina[ii].SetActive(false);
        ii++;
        if (ii >= 0)
        {
            tut.atras.gameObject.SetActive(true);
        }

        if (ii < tut.pagina.Length)
        {
            tut.pagina[ii].SetActive(true);
        }
        else
        {
            TerminarTutorial();
        }
    }

    public void AtrasPagina()
    {
        tut.pagina[ii].SetActive(false);
        ii--;
        print(ii);
        if (ii > 0)
        {
            tut.pagina[ii].SetActive(true);
        }
        else
        {
            tut.pagina[ii].SetActive(true);
            tut.atras.gameObject.SetActive(false);
            print("why");
        }
    }

    public void TerminarTutorial()
    {
        pausa.noPausar = false;
        tut.DesactivarTutorial();
        pausa.Pausar();
    }

    void Reiniciar()
    {
        ii = 0;

        IniciarTutorial();
    }
}
