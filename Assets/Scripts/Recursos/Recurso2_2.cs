﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recurso2_2 : Recurso2 {

    public override void Cambios()
    {
        nombreDeRecurso = "Madera 2";
        numeroDeRecurso = 2;
        tiempoDeEspera = 8;
        TipoDeArbol();

        smoke = GameObject.Find("Smoke1");
        smoke.GetComponent<Renderer>().enabled = false;
    }

    public override void TipoDeArbol()
    {
        if (numeroDeMejora >= 3)
        {
            sePuedeRecoger = true;
            return;
        }
        arbolTipo = 2;
        sePuedeRecoger = false;
    }
      
}
