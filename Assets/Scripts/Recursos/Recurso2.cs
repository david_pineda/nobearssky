﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recurso2 : Recursos
{

    public int arbolTipo;

    public override void Cambios()
    {
        nombreDeRecurso = "Madera";
        numeroDeRecurso = 2;
        tiempoDeEspera = 5;
        //GetComponent<Renderer>().material.color = Color.green;
        TipoDeArbol();

        smoke = GameObject.Find("Smoke1");
        smoke.GetComponent<Renderer>().enabled = false;
    }

    public override void EnviarEvento()
    {
        EventoArriba(numeroDeRecurso);
    }

    public virtual void TipoDeArbol()
    {
        arbolTipo = 0;
        sePuedeRecoger = true;
    }


}
