﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class Recursos : MonoBehaviour {

    public delegate void RecursoRecogido(int recogidos);
    public static event RecursoRecogido EnRecogida1;

    public delegate void Parado();
    public static event Parado EnParada;
    public static event Parado EnSalida;

    public delegate void Recogiendo();
    public static event Recogiendo EnRecogiendoRecurso;
    public static event Recogiendo EnTerminarDeRecoger;
    public static event Recogiendo EnNoSePuede;

    public int numeroDeRecurso;
    public string nombreDeRecurso;

    public GameObject smoke;

    //Text recurso;
    //Slider sliderRecoleccion;

    private UILord ui;

    public TimerRecursos tr;
    public float tiempoDeEspera;

    private bool recogiendo;
    private bool dentroDeRecurso;

    public float mejora;
    public int numeroDeMejora;

    public bool sePuedeRecoger; //para los arboles


    private bool existir;
    private bool funcionar;
    private bool enPausa;

    public AudioSource sonar;
    public AudioClip sonido;

    public float z;

    private PausaLord pausa;

    private static GameObject triggered;
    private bool dontPayAttentionToMe;

    private float test;
    private int aa;

    Collider[] colliders;
    bool[] collidersOr;

    void Awake()
    {
        sonar = GetComponent<AudioSource>();

        existir = true;
        funcionar = true;
    }

    void Start() {
        colliders = gameObject.GetComponents<Collider>();
        collidersOr = new bool[colliders.Length];
        for (int i = 0; i < collidersOr.Length; i++)
        {
            collidersOr[i] = colliders[i].isTrigger;
        }
        test = 0;
        aa = 0;
        triggered = null;
        dontPayAttentionToMe = false;
        sePuedeRecoger = true;
        z = 0;
        Cambios();
        tr = GameObject.FindWithTag("GameController").GetComponent<TimerRecursos>();
        pausa = PausaLord.Instancia;

        //sliderRecoleccion = GameObject.Find("SliderRecursos").GetComponent<Slider>();

        //recurso = GameObject.Find("RecursoText").GetComponent<Text>();


        ui = UILord.Instancia;
        ui.recurso.text = "";

        recogiendo = false;
        dentroDeRecurso = false;
        MovimientoPersonaje.EnInteraccion += IniciarRecogida;
        UpgradeManager.EnMejoraTaladro += Mejora;


        mejora = 1; //no hay mejora de velocidad
        numeroDeMejora = 1; //igual a b, no hay mejora inicialmente

        enPausa = false;

        Reinicio.EnReinicioYa += Reiniciar;
    }

    public virtual void Cambios ()
    {
        nombreDeRecurso = "Basic";
        numeroDeRecurso = 0;
        tiempoDeEspera = 1;
        //GetComponent<Renderer>().material.color = Color.blue;

        smoke = GameObject.Find("Smoke1");
        smoke.GetComponent<Renderer>().enabled = false;
        z = 1;
    }
	
    void Mejora()
    {
        mejora *= 1.3f;
        tr.speed = mejora;
        numeroDeMejora++;
        Cambios();
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && triggered != null)
        {
            dontPayAttentionToMe = true;
        }
        if (other.CompareTag("Player") && triggered == null)
        {
            triggered = other.gameObject;
            dentroDeRecurso = true;
            if (funcionar)
            {
                ui.presionar.text = "Presiona X";
            }
        }
    }

    //En OnTriggerStay iba lo que ahora está en Fixed Update

    void FixedUpdate()
    {

        if (dentroDeRecurso == true && recogiendo == true && !dontPayAttentionToMe)
        {
            ui.sliderRecoleccion.value -= Time.deltaTime * mejora;

            EnRecogiendoRecurso(); //Lo recibe SonarEnRecogida, que está en la sombra del personaje  y MovimientoPersonaje para animar
            ui.presionar.text = "";

            GamePad.SetVibration(0, 0.5f, 0.5f);

            if (tr.rTimers[numeroDeRecurso] >= tiempoDeEspera)
            {
                GamePad.SetVibration(0, 0.0f, 0.0f);

                funcionar = false;
                IniciarSonido();

                ui.recurso.text = nombreDeRecurso + " recogido/a";
                tr.iniciar = false;
                tr.rTimers[numeroDeRecurso] = 0;
                EnviarEvento();
                ui.sliderRecoleccion.gameObject.SetActive(false);

                recogiendo = false;
                dentroDeRecurso = false;
                EnSalida(); //Lo recibe MovimientoPersonaje
                EnTerminarDeRecoger(); //Lo recibe SonarEnRecogida y MovimientoPersonaje para animar

                gameObject.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0);
                for (int i = 0; i < colliders.Length; i++)
                {
                    collidersOr[i] = colliders[i].isTrigger;
                    if (collidersOr[i] == false)
                    {
                        collidersOr[i] = false;
                    }
                    colliders[i].isTrigger = true;
                }

                smoke.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
                smoke.GetComponent<Renderer>().enabled = true;
                Vector3 tf;
                tf = new Vector3(transform.position.x, transform.position.y + 1.1f, transform.position.z + z);
                smoke.transform.position = tf;
            }
        }

        if (funcionar == false)
        {
            smoke.GetComponent<Renderer>().material.color = new Color(1, 1, 1, smoke.GetComponent<Renderer>().material.color.a - Time.deltaTime);
            ui.presionar.text = "";
        }

        if (!sonar.isPlaying && sonar.clip == sonido && !enPausa)
        {
            existir = false;
        }

        if (!existir)
        {
            smoke.GetComponent<Renderer>().enabled = false;
            gameObject.SetActive(false);
        }


        if (ui.sliderRecoleccion.IsActive() == true) //arreglo de bug extraño
        {
            if (ui.sliderRecoleccion.value == test)
            {
                aa++;
                if (aa >= 1000)
                {
                    print("ah, damnit");
                    ui.sliderRecoleccion.gameObject.SetActive(false);
                    aa = 0;
                    EnTerminarDeRecoger();
                    EnSalida();
                }
            }
        }
    }

    void Update()
    {
        if (pausa.setPausa == 1)
        {
            GamePad.SetVibration(0, 0.0f, 0.0f);
            sonar.Pause();
            enPausa = true;
        }
        else if (pausa.setPausa == 0)
        {
            sonar.UnPause();
            enPausa = false;
        }
    }


    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            triggered = null;
            if (dontPayAttentionToMe)
            {
                dontPayAttentionToMe = false;
            }

            ui.sliderRecoleccion.gameObject.SetActive(false);

            tr.iniciar = false;
            tr.rTimers[numeroDeRecurso] = 0;

            recogiendo = false;
            dentroDeRecurso = false;
            EnSalida();

            ui.presionar.text = "";
        }
    }



    public virtual void EnviarEvento()
    {
        EventoArriba(numeroDeRecurso);
    }

    protected virtual void EventoArriba(int hijo)
    {
        EnRecogida1(hijo);
    }


    private void IniciarCuenta()
    {
        if (dentroDeRecurso == true && sePuedeRecoger == true && recogiendo == true)
        {
            tr.iniciar = true;
            tr.activo = numeroDeRecurso;

            ui.sliderRecoleccion.gameObject.SetActive(true);
            ui.sliderRecoleccion.maxValue = tiempoDeEspera;
            ui.sliderRecoleccion.value = tiempoDeEspera;
            test = ui.sliderRecoleccion.value;
        }
    }

    private void IniciarRecogida()
    {
        if (funcionar == true)
        {
            if (dentroDeRecurso == true && recogiendo == false && sePuedeRecoger == true)
            {
                recogiendo = true;
                IniciarCuenta();

                EnParada();
            }
            else if (sePuedeRecoger == false && recogiendo == false && dentroDeRecurso == true)
            {

                EnRecogiendoRecurso();
                ui.invalido.color = new Color(1, 1, 1, 1);
                ui.invalido.text = "No se puede recoger aún";
                EnNoSePuede();
                EnTerminarDeRecoger();
            }
        }
    }

    private void IniciarSonido() //Cuando se recoge el recurso
    {
        CuadrarSonido();
        sonar.Play();
    }

    public virtual void CuadrarSonido()
    {
        sonar.clip = sonido;
        sonar.loop = false;
        sonar.volume = 0.05f;
        sonar.maxDistance = 10;
        sonar.pitch = 1.5f;
    }




    void Reiniciar()
    {
        sonar.clip = null;
        existir = true;
        funcionar = true;

        triggered = null;
        dontPayAttentionToMe = false;
        sePuedeRecoger = true;
        z = 0;
        Cambios();

        ui.recurso.text = "";
        ui.presionar.text = "";

        recogiendo = false;
        dentroDeRecurso = false;

        mejora = 1; //no hay mejora de velocidad
        numeroDeMejora = 1; //igual a b, no hay mejora inicialmente

        enPausa = false;

        tr.rTimers[numeroDeRecurso] = 0;
        tr.speed = mejora;
        tr.iniciar = false;

        ui.sliderRecoleccion.gameObject.SetActive(false);
        gameObject.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].isTrigger = collidersOr[i];
        }


        this.gameObject.SetActive(true);
    }
}
