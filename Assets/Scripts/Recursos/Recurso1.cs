﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recurso1 : Recursos {

    //public static event RecursoRecogido EnRecogida1;

    public override void Cambios()
    {
        nombreDeRecurso = "Miel";
        numeroDeRecurso = 1;
        tiempoDeEspera = 2;
        //GetComponent<Renderer>().material.color = Color.yellow;

        smoke = GameObject.Find("Smoke2");
        smoke.GetComponent<Renderer>().enabled = false;
        z = 2;
    }

    public override void EnviarEvento()
    {
        EventoArriba(numeroDeRecurso);
    }

    public override void CuadrarSonido()
    {
        sonar.clip = sonido;
        sonar.loop = false;
        sonar.volume = 0.2f;
        sonar.maxDistance = 10;
        sonar.pitch = 1.5f;
    }
}
