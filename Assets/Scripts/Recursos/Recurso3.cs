﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recurso3 : Recursos {

    public override void Cambios()
    {
        nombreDeRecurso = "Hielo";
        numeroDeRecurso = 3;
        tiempoDeEspera = 3;

        smoke = GameObject.Find("Smoke2");
        smoke.GetComponent<Renderer>().enabled = false;
        z = 1;
    }

    public override void EnviarEvento()
    {
        EventoArriba(numeroDeRecurso);
    }

    public override void CuadrarSonido()
    {
        sonar.clip = sonido;
        sonar.loop = false;
        sonar.volume = 0.23f;
        sonar.maxDistance = 10;
        sonar.pitch = 2.3f;
    }
}
