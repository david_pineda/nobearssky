﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerRecursos : MonoBehaviour {



    public float[] rTimers;
    public int activo;
    public bool iniciar;

    public float speed = 1;

    void Start()
    {
        rTimers = new float[4];
        for (int i = 0; i < rTimers.Length; i++)
        {
            rTimers[i] = 0;
        }
    }

	void FixedUpdate () 
    {
        if (iniciar)
        {
            switch (activo)
            {
                case 0:
                    Debug.Log("uh oh");
                    break;

                case 1:
                    rTimers[1] += Time.deltaTime * speed;
                    break;

                case 2:
                    rTimers[2] += Time.deltaTime * speed;
                    break;

                case 3:
                    rTimers[3] += Time.deltaTime * speed;
                    break;

                default:
                    Debug.Log("uh oh");
                    break;
            }
        }
	}
}
