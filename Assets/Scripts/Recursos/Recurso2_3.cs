﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recurso2_3 : Recurso2 {

    public override void Cambios()
    {
        nombreDeRecurso = "Madera 3";
        numeroDeRecurso = 2;
        tiempoDeEspera = 10;
        TipoDeArbol();

        smoke = GameObject.Find("Smoke1");
        smoke.GetComponent<Renderer>().enabled = false;
    }

    public override void TipoDeArbol()
    {
        if (numeroDeMejora >= 5)
        {
            sePuedeRecoger = true;
            return;
        }
        arbolTipo = 3;
        sePuedeRecoger = false;
    }
}
