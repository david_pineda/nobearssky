﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour {

    /// <summary>
    /// Este Script controla los textos de los recursos
    /// </summary>


    //public Text contador;
    //public Text contador2;

    public int recurso1Contador = 0; //miel
    public int recurso2Contador = 0; //madera
    public int recurso3Contador = 0; //hielo

    private UILord ui;
    private UpgradeManager upgrade;

    public void Start()
    {
        ui = UILord.Instancia;

        UpdateTexto();

        Recursos.EnRecogida1 += AumentarContador;
        Reinicio.EnReiniciando += Reiniciar;
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.I) && Input.GetKey(KeyCode.U))
        {
            AumentarContador(1);
            AumentarContador(2);
            AumentarContador(3);
        }
    }


    void AumentarContador(int recurso)
    {
        switch (recurso)
        {
            case 1:
                recurso1Contador++; //Miel
                break;

            case 2:
                recurso2Contador += Random.Range(1, 4); //Madera
                break;

            case 3:
                recurso3Contador++; //Agua
                break;

            default:
                Debug.Log("Error");
                break;
        }

        UpdateTexto();
    }

    public void UpdateTexto()
    {
        ui.contador.text = "X " + recurso1Contador.ToString(); //Mi
        ui.contador2.text = "X " + recurso2Contador.ToString(); //Ma
        ui.contador3.text = "X " + recurso3Contador.ToString(); //Hi

        ui.menuRecurso[0].text = "X " + recurso1Contador.ToString(); //Mi
        ui.menuRecurso[1].text = "X " + recurso2Contador.ToString(); //Ma
        ui.menuRecurso[2].text = "X " + recurso3Contador.ToString(); //Hi
    }


    void Reiniciar()
    {
        recurso1Contador = 0; //miel
        recurso2Contador = 0; //madera
        recurso3Contador = 0; //hielo
        UpdateTexto();
    }
       
}
