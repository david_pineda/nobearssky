﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recurso2_1 : Recurso2 {

    public override void Cambios()
    {
        nombreDeRecurso = "Madera 1";
        numeroDeRecurso = 2;
        tiempoDeEspera = 5;
        TipoDeArbol();

        smoke = GameObject.Find("Smoke1");
        smoke.GetComponent<Renderer>().enabled = false;
    }

    public override void TipoDeArbol()
    {
        arbolTipo = 1;
        sePuedeRecoger = true;
    }
}
