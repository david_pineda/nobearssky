﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarEnRecogida : MonoBehaviour {

    [SerializeField]
    private AudioSource sonar;
    public AudioClip taladroSonido;

    private PausaLord pausa;

    private bool fading;

	void Start ()
    {
        Recursos.EnRecogiendoRecurso += SonarTaladro;
        Recursos.EnTerminarDeRecoger += PararTaladro;


        pausa = PausaLord.Instancia;
        sonar = this.GetComponent<AudioSource>();
	}
	
    void Update()
    {
        if (fading == true)
        {
            FadeOut();
        }

        if (pausa.setPausa == 1)
        {
            sonar.Pause();
        }
        else if (pausa.setPausa == 0)
        {
            sonar.UnPause();
        }
    }

    void SonarTaladro()
    {
        if (sonar == null)
        {
            sonar = this.GetComponent<AudioSource>();
        }
        if (!sonar.isPlaying)
        {
            fading = false;

            sonar.clip = taladroSonido; //Se pone el clip de sonido
            sonar.loop = true;
            sonar.volume = 0.05f;
            //sonar.pitch = 1;

            sonar.Play();
        }
    }

    void PararTaladro()
    {
        if (sonar.isPlaying)
        {
            fading = true;
        }
    }

    void FadeOut()
    {
        sonar.volume -= Time.deltaTime/3;

        if (sonar.volume <= 0)
        {
            sonar.Stop();
        }
    }
}
