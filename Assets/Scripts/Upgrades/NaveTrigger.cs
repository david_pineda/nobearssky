﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaveTrigger : MonoBehaviour {

    private bool dentroDeNave, entrando;

    public delegate void NaveMenu();
    public static event NaveMenu EnMenu; //Se envía a MenuLord
    public static event NaveMenu EnSalida;
    public static event NaveMenu EnEntrada;

    public static event NaveMenu EnTimeWarp; //se envía a TimeWarpLord

    private IndicadorDiaNoche indicador;

    private UILord ui;

    void Start ()
    {
        ui = UILord.Instancia;
        dentroDeNave = false;
        entrando = false;

        MovimientoPersonaje.EnInteraccion += IniciarMenu;

        indicador = IndicadorDiaNoche.Instancia; //Instancia del Singleton Indicador
	}


    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            dentroDeNave = true;
            ui.presionar.text = "Presiona X para entrar";
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            EnEntrada();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            dentroDeNave = false;
            entrando = false;
            ui.presionar.text = "";
        }
    }




    public void IniciarMenu()
    {
        if (dentroDeNave == true && entrando == false)
        {
            entrando = true;
            EnMenu(); //se envía a menulord y a movimiento personaje, pero solo para el sonido
            ui.sliderRecoleccion.gameObject.SetActive(false);
        }
        else if (dentroDeNave == true && entrando == true)
        {
            entrando = false;
            EnSalida(); //se envía a menulord y a movimiento personaje, pero solo para el sonido
            ui.sliderRecoleccion.gameObject.SetActive(false);

            if (indicador.dia == false)
            {
                //Ha pasado 1 dia
                EnTimeWarp();
            }
        }
    }
}
