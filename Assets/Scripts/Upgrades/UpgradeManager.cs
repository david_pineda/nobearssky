﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeManager {

    public static UpgradeManager instancia = null;

    private UILord ui;
    private MisionesLord misiones;

    private int[] RecursosNecesarios;
    private int[] RecursoNecesarioActual;
    private string[] recursoAUsar;
    private int usando;

    private ResourceManager manager;

    public delegate void Mejora(); //Evento Que envía mejoras
    public static event Mejora EnMejoraTaladro;
    public static event Mejora EnMejoraEnergia;
    public static event Mejora EnMejoraEscudo;

    int a, b, c;

    public bool sePuedeMejorar;

    //private GameObject[] Player; // referencia al jugador 
    //public int mejoraSpeed = 2;// cuanto aumenta la velocidad cuendo mejora 

    public static UpgradeManager Instancia
    {
        get
        {
            if (instancia == null)
            {
                instancia = new UpgradeManager();
            }
            return (instancia);
        }
    }

    private UpgradeManager()
    {
        misiones = MisionesLord.Instancia;
        recursoAUsar = new string[3];
        recursoAUsar[0] = "Miel";
        recursoAUsar[1] = "Madera";
        recursoAUsar[2] = "Hielo";
        usando = 0;

        sePuedeMejorar = false;

        ui = UILord.Instancia;

        RecursosNecesarios = new int[5];
        RecursoNecesarioActual = new int[4];

        RecursosNecesarios[0] = 3;
        RecursosNecesarios[1] = 5;
        RecursosNecesarios[2] = 8;
        RecursosNecesarios[3] = 11;
        RecursosNecesarios[4] = 15;

        for (int i = 0; i < RecursoNecesarioActual.Length; i++)
        {
            RecursoNecesarioActual[i] = RecursosNecesarios[0];
        }
       
        manager = GameObject.FindWithTag("GameController").GetComponent<ResourceManager>();
        a = b = c = 1;

        ui.recurso1.text = RecursosNecesarios[0].ToString();
        ui.recurso2.text = RecursosNecesarios[0].ToString();
        ui.recurso3.text = RecursosNecesarios[0].ToString();

        // se obtienen las instancias de objetos con la tag player 
        //Player = GameObject.FindGameObjectsWithTag("Player");
    }

    public void UpgradeRealizado(int aMejorar)
    {
        switch (aMejorar)
        {
            case 1:
                UpgradeMiel();
                break;

            case 2:
                UpgradeMadera();

                break;

            case 3:
                UpgradeHielo();

                break;

            default:
                break;
        }
    }


    private void CambiarMision(int revisar)
    {
        if (recursoAUsar[0] == "" && recursoAUsar[1] == "" && recursoAUsar[2] == "")
        {
            usando = -1;
            misiones.CambiarMisionExtra("", "");
            return;
        }

        revisar = Random.Range(0, 3);
        while (revisar == usando)
        {
            revisar = Random.Range(0, 3);
        }
        usando = revisar;
        PublicarMision();
    }

    private void PublicarMision()
    {
        if (recursoAUsar[usando] == "")
        {
            CambiarMision(usando);
        }
        switch (usando)
        {
            case 0:
                misiones.CambiarMisionExtra(recursoAUsar[0], "Energía");
                break;

            case 1:
                misiones.CambiarMisionExtra(recursoAUsar[1], "Taladro");
                break;

            case 2:
                misiones.CambiarMisionExtra(recursoAUsar[2], "Escudo");
                break;

            default:
                break;
        }
    }

    private bool RevisarRecursos(int recurso, int _i)
    {

        switch (recurso)
        {
            case 1:
                if (manager.recurso1Contador >= RecursosNecesarios[_i - 1])
                {
                    sePuedeMejorar = true;
                    return true;
                }
                break;

            case 2:
                if (manager.recurso2Contador >= RecursosNecesarios[_i - 1])
                {
                    sePuedeMejorar = true;
                    return true;
                }
                break;

            case 3:
                if (manager.recurso3Contador >= RecursosNecesarios[_i - 1])
                {
                    sePuedeMejorar = true;
                    return true;
                }
                break;

            default:
                break;
        }

        sePuedeMejorar = false;
        return false;
    }


    private void UpgradeMiel() //Mejora Energía
    {
        if (a <= 5)
        {
            if (RevisarRecursos(1, a))
            {
                manager.recurso1Contador -= RecursosNecesarios[a - 1];
                a++;
                if (a <= 5)
                {
                    ui.recurso1.text = RecursosNecesarios[a - 1].ToString();
                }
                manager.UpdateTexto();

                // aqui se aplica la mejora 
                ////Player[0].GetComponent<MovimientoPersonaje>().speed += mejoraSpeed;
                EnMejoraEnergia();

                ui.algoSucede.text = "Energía Mejorada";
                if (usando == 0)
                {
                    CambiarMision(usando);
                }
                //Debug.Log("Energía Mejorada");
            }
            else if (!RevisarRecursos(1, a))
            {
                ui.algoSucede.text = "Energía Aun no puede mejorarse";
                //Debug.Log("Energía No Puede Ser Mejorada");
                //recursoAUsar[0] = "";
            }
        }
        if (a > 5)
        {

            ui.recurso1.text = "X";
            ui.algoSucede.text = "Energía Mejorada al Máximo";
            //Debug.Log("Energía no se puede mejorar más");
            sePuedeMejorar = false;
            ui.botonesMenu[0].interactable = false;
            recursoAUsar[0] = "";
            if (usando == 0)
            {
                CambiarMision(usando);
            }
            //CambiarMision(0);
        }
    }

    private void UpgradeMadera() //Mejora Taladro
    {
        if (b <= 5)
        {
            if (RevisarRecursos(2, b) && b <= 5)
            {
                manager.recurso2Contador -= RecursosNecesarios[b - 1];
                b++;
                if (b <= 5)
                {
                    ui.recurso2.text = RecursosNecesarios[b - 1].ToString();
                }
                manager.UpdateTexto();

                EnMejoraTaladro(); //Envía la mejora del taladro a Recursos

                ui.algoSucede.text = "Taladro Mejorado";
                if (usando == 1)
                {
                    CambiarMision(usando);
                }
                //Debug.Log("Taladro Mejorado");
            }
            else if (!RevisarRecursos(2, b) && b <= 5)
            {
                ui.algoSucede.text = "Taladro Aun no puede mejorarse";
                //Debug.Log("Taladro No Puede Ser Mejorado");
                //recursoAUsar[1] = "";
            }
        }
        if (b > 5)
        {
            ui.recurso2.text = "X";
            ui.algoSucede.text = "Taladro Mejorado al Máximo";
            //Debug.Log("Taladro no se puede mejorar más");
            sePuedeMejorar = false;
            ui.botonesMenu[1].interactable = false;
            recursoAUsar[1] = "";
            if (usando == 1)
            {
                CambiarMision(usando);
            }
            //CambiarMision(1);
        }
    }

    private void UpgradeHielo() //Mejora Escudo
    {
        if (c <= 5)
        {
            if (RevisarRecursos(3, c))
            {
                manager.recurso3Contador -= RecursosNecesarios[c - 1];
                c++;
                if (c <= 5)
                {
                    ui.recurso3.text = RecursosNecesarios[c - 1].ToString();
                }
                manager.UpdateTexto();

                EnMejoraEscudo(); //Se envía el evento de mejora a EscudoLord

                ui.algoSucede.text = "Escudo Mejorado";
                if (usando == 2)
                {
                    CambiarMision(usando);
                }
                //Debug.Log("Escudo Mejorado");
            }
            else if (!RevisarRecursos(3, c))
            {
                ui.algoSucede.text = "Escudo Aun no puede ser mejorado";
                //Debug.Log("Escudo No Puede Ser Mejorado");
                //recursoAUsar[2] = "";
            }
        }
        if (c > 5)
        {
            ui.recurso3.text = "X";
            ui.algoSucede.text = "Escudo Mejorado al Máximo";
            //Debug.Log("Escudo no se puede mejorar más");
            sePuedeMejorar = false;
            ui.botonesMenu[2].interactable = false;
            recursoAUsar[2] = "";
            if (usando == 2)
            {
                CambiarMision(usando);
            }
            //CambiarMision(2);
        }
    }



    public void Reiniciar()
    {
        recursoAUsar[0] = "Miel";
        recursoAUsar[1] = "Madera";
        recursoAUsar[2] = "Hielo";
        usando = 0;

        sePuedeMejorar = false;

        RecursosNecesarios[0] = 3;
        RecursosNecesarios[1] = 5;
        RecursosNecesarios[2] = 8;
        RecursosNecesarios[3] = 11;
        RecursosNecesarios[4] = 15;

        for (int i = 0; i < RecursoNecesarioActual.Length; i++)
        {
            RecursoNecesarioActual[i] = RecursosNecesarios[0];
        }
        a = b = c = 1;

        ui.recurso1.text = RecursosNecesarios[0].ToString();
        ui.recurso2.text = RecursosNecesarios[0].ToString();
        ui.recurso3.text = RecursosNecesarios[0].ToString();

        ui.botonesMenu[0].interactable = true;
        ui.botonesMenu[1].interactable = true;
        ui.botonesMenu[2].interactable = true;
    }
}
