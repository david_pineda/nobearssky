﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrader : MonoBehaviour {


    UpgradeManager upgrade;

    private void Start()
    {
        upgrade = UpgradeManager.Instancia;
    }

    public void SendUpgrade (int recursoUsado)
    {
        upgrade.UpgradeRealizado(recursoUsado);
    }
}
