﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeligroDeNocheLord : MonoBehaviour {

    private IndicadorDiaNoche indicador;

    private bool noche;

	void Start()
    {
        indicador = IndicadorDiaNoche.Instancia;
        noche = false;
    }

    void Update()
    {
        if (indicador.dia == false && noche == false)
        {
            noche = true;
            NochePeligro();
        }

        if (indicador.dia == true && noche == true)
        {
            noche = false;
            DiaTranquilo();
        }
    }

    void NochePeligro()
    {
        print("spooky scary skeletons");
    }

    void DiaTranquilo()
    {
        print("im a brave boy");
    }

}
