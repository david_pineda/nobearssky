﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicadorDiaNoche {

    public bool dia;
    public int diasPasados;
    public bool warp;

    public static IndicadorDiaNoche instancia = null;

    public static IndicadorDiaNoche Instancia
    {
        get
        {
            if (instancia == null)
            {
                instancia = new IndicadorDiaNoche();
            }
            return (instancia);
        }
    }

    public IndicadorDiaNoche()
    {
        dia = true;
        diasPasados = 0;
        warp = false;
    }

    public void Reiniciar()
    {
        dia = true;
        diasPasados = 0;
        warp = false;
    }





}
