﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotacionDiaNoche : MonoBehaviour {
    public float TiempoDia;
    public float angulo,angulotemp;
    //public bool activo;
    private PausaLord pausa;

    private IndicadorDiaNoche indicador;

    // peligro de noche 
    public int cantidadEnemigosNoche = 10;
    public GameObject[] EnemigosNoche;
    public int longitud;
    public GameObject Enemigo;
    public GameObject[] puntos;
    public bool Enemigoscreados = false;

    public delegate void Dia();
    public static event Dia EnPasoUnaNoche;

    void Start () {
        angulotemp = 180; angulo = 180 / TiempoDia;
        //activo = true;
        
        pausa = PausaLord.Instancia;
        longitud = EnemigosNoche.Length;
        indicador = IndicadorDiaNoche.Instancia; //Busca el singleton que controla el estado dia o noche
        EnemigosNoche = new GameObject[cantidadEnemigosNoche];
        puntos = GameObject.FindGameObjectsWithTag("Point");
    }

    // Update is called once per frame
    void Update()
    {
        if (pausa.setPausa == 0)
        {
            angulotemp += angulo;
            if (angulotemp <= 180) //noche
            {
                indicador.dia = false;
                // print("noche");
                angulotemp += angulo;

                transform.rotation = Quaternion.AngleAxis(0.0f - angulotemp, Vector3.right);
                if (!Enemigoscreados)
                {
                    crearEnemigos();
                }
               

            }
            else //dia 
            {
                if (Enemigoscreados)
                {
                    EliminarEnemigos();
                }



                indicador.dia = true;
                //   print(angulotemp);
                transform.rotation = Quaternion.AngleAxis(0.0f - angulotemp, new Vector3(1, 0, 0));
            }
            if (angulotemp >= 360)
            {
                angulotemp = 0;
            }
        }
    }
    public void setAngulo(int angulonew) {
        if ((angulonew <= 360) && (angulonew > 0))
        {
            angulotemp = angulonew;
        }
    }


    public void crearEnemigos() {
        int numero = 0;
        Enemigoscreados = true;
        print("creando enemigos");
        for (int i = 0; i < cantidadEnemigosNoche; i++)
        {
            numero = (int)Random.Range(0.0f, puntos.Length);
            // agent.SetDestination(puntos[numero].transform.position);
            Vector3 pos = new Vector3(puntos[numero].transform.position.x, 1.095105f, puntos[numero].transform.position.z);  // supongo que es x,y,z
            //EnemigosNoche.SetValue(Instantiate(Enemigo, pos, this.transform.rotation), i);
            EnemigosNoche[i]=  Instantiate(Enemigo, pos, this.transform.rotation); // posision y = 1.095105 
            EnemigosNoche[i].GetComponent<EnemigoV2>().ladistancia =30;
        }
         


    }
    public void EliminarEnemigos() { // hay que cambiar este metodo para que no los elimine tan asi 
        Enemigoscreados = false;

        if (indicador.warp == false)
        {
            indicador.diasPasados++;
            EnPasoUnaNoche();
            //print("paso un dia");
        }

        foreach (var item in EnemigosNoche) // elemina los enemigos creados por la noche 
        {
            Destroy(item); // los elimina del todo , esto consume recursos buscar una manera mas optima 
        }

    }


}
