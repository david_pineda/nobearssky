﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeWarpLord : MonoBehaviour {

    private rotacionDiaNoche diaNoche;
    private IndicadorDiaNoche indicador;
    private Perdida ocurreAlgo;

    private Color alpha;
    private Color beta;
    private Color theta;
    float temp;
    private bool quitarText;

    private UILord ui;
    private bool blackness;

    public delegate void TimeWarper();
    public static event TimeWarper EnPasoDelTiempo; //Se envía a TimerLord
    public static event TimeWarper EnPierdeUnDia; //Se envía a MovimientoPersonaje

    void Start()
    {
        NaveTrigger.EnTimeWarp += Oscuridad;

        indicador = IndicadorDiaNoche.Instancia;
        diaNoche = GetComponent<rotacionDiaNoche>();
        ocurreAlgo = Perdida.Instancia;

        blackness = false;


        ui = UILord.Instancia;
        alpha = Color.black;
        alpha.a = 0;
        ui.black.color = alpha;
        quitarText = false;
        indicador.diasPasados = 0;
        beta = ui.dias.color;
        beta.a = 0;
        ui.dias.color = beta;
        theta = beta;
        temp = 1;


        MovimientoPersonaje.EnFaltaDeEnegia += Oscuridad;
        Reinicio.EnReiniciando += Reiniciar;
        rotacionDiaNoche.EnPasoUnaNoche += PonerTexto;
    }

    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            TimeWarpInverse();
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            Oscuridad();
        }

        if (alpha.a > 0)
        {
            if (blackness == true) //fade out
            {
                alpha.a += 0.1f;
            }

            if (alpha.a >= 2 && blackness == true)
            {
                blackness = false;
                EnPasoDelTiempo();
                TimeWarpAgain(); //Se hace esto para que no se note tanto el cambio entre un dia y otro
                quitarText = true;

                if (ocurreAlgo.accidente == true)
                {
                    ocurreAlgo.volverANormalidad = true;
                    ocurreAlgo.accidente = false;
                    EnPierdeUnDia();
                }
            }

            if (blackness == false) //fade in
            {
                alpha.a -= 0.1f;
            }
            ui.black.color = alpha;
        }

        if (alpha.a <= 0 && quitarText)
        {
            if (beta.a > 0)
            {
                beta.a -= Time.deltaTime;
                ui.dias.color = beta;
            }
            else
            {
                quitarText = false;
                indicador.warp = false;
            }
        }

        if (theta.a > 0)
        {
            temp -= 0.1f;
            if (temp <= 0)
            {
                theta.a -= Time.deltaTime;
                ui.dias.color = theta;
            }
        }
        else
        {
            temp = 1;
        }

    }

    void TimeWarpAgain()
    {
        diaNoche.angulotemp = 180;
        indicador.diasPasados++;
        if (indicador.diasPasados == 1)
        {
            beta.a = 1;
            ui.dias.color = beta;
            ui.dias.text = "Día " + indicador.diasPasados.ToString();
        }
        else
        {
            beta.a = 1;
            ui.dias.color = beta;
            ui.dias.text = "Día " + indicador.diasPasados.ToString();
        }
    }

    void PonerTexto()
    {
        if (indicador.diasPasados == 1)
        {
            theta.a = 1;
            ui.dias.color = theta;
            ui.dias.text = "Día " + indicador.diasPasados.ToString();
        }
        else
        {
            theta.a = 1;
            ui.dias.color = theta;
            ui.dias.text = "Día " + indicador.diasPasados.ToString();
        }
    }

    void Oscuridad() 
    {
        indicador.warp = true;
        blackness = true;
        alpha.a = 0.1f;
    }

    void TimeWarpInverse()
    {
        diaNoche.angulotemp = 360;
    }

    void Reiniciar()
    {
        diaNoche.angulotemp = 180;

        blackness = false;
        alpha.a = 0;
        ui.black.color = alpha;
        quitarText = false;
        indicador.diasPasados = 0;
        beta = ui.dias.color;
        beta.a = 0;
        ui.dias.color = beta;
    }
}
