﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzMoradaRotacion : MonoBehaviour {

    public float TiempoDia;
    public float angulo, angulotemp;
    //public bool activo;
    private PausaLord pausa;

    private IndicadorDiaNoche indicador;


    void Start()
    {
        angulotemp = 00; angulo = 0 / TiempoDia;
        //activo = true;
        pausa = PausaLord.Instancia;

        indicador = IndicadorDiaNoche.Instancia; //Busca el singleton que controla el estado dia o noche
    }

    // Update is called once per frame
    void Update()
    {
        if (pausa.setPausa == 0)
        {
            angulotemp += angulo;
            if (angulotemp <= 180) //noche
            {
                indicador.dia = false;
                // print("noche");
                angulotemp += angulo;

                transform.rotation = Quaternion.AngleAxis(0.0f - angulotemp, Vector3.right);
            }
            else //dia 
            {
                indicador.dia = true;
                //   print(angulotemp);
                transform.rotation = Quaternion.AngleAxis(0.0f - angulotemp, new Vector3(1, 0, 0));
            }
            if (angulotemp >= 360)
            {
                angulotemp = 0;
            }
        }
    }
    public void setAngulo(int angulonew)
    {
        if ((angulonew <= 360) && (angulonew > 0))
        {
            angulotemp = angulonew;
        }
    }
}

