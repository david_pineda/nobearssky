﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Desaparecer : MonoBehaviour {

    UILord ui;
    Color alpha;
    Text yo;

    // Use this for initialization
    void Start ()
    {
        yo = GetComponent<Text>();
        alpha = yo.color;
        alpha.a = 1;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
		if (yo.color.a > 0)
        {
            alpha.a -= Time.deltaTime;
            yo.color = alpha;
   
            if (alpha.a <= 0)
            {
                alpha.a = 1;
            }
        }
	}
}
