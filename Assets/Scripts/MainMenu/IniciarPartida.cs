﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class IniciarPartida : MonoBehaviour {

    [SerializeField]
    private Button play;

    void Awake()
    {
        if (play == null)
        play = GameObject.Find("InicioBoton").GetComponent<Button>();

        play.onClick.AddListener(JugarNivel);
    }

    public void JugarNivel()
    {
        StartCoroutine(DontActuallyStart());
        play.interactable = false;
    }

    IEnumerator DontActuallyStart()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("LoadingScene", LoadSceneMode.Single);
    }
}
