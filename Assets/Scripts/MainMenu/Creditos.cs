﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Creditos : MonoBehaviour {

    public GameObject menu, creditos;

    public Button creditosBoton, regresar;

    private Animator animMenu;

    private void Awake()
    {
        Time.timeScale = 1;

        creditosBoton = GameObject.Find("CreditosBoton").GetComponent<Button>();

        creditosBoton.onClick.AddListener(IniciarCreditos);

        regresar = GameObject.Find("BtnRegresar").GetComponent<Button>();

        regresar.onClick.AddListener(VolverMenu);

        creditos.SetActive(false);
    }

    public void IniciarCreditos()
    {
        menu.GetComponent<Animator>().SetBool("Creditos",true);//Fade Out Menu

        creditos.SetActive(true);
        creditos.GetComponent<Animator>().SetBool("Menu", false);//Fade In Creditos

    }

    public void VolverMenu()
    {
        creditos.GetComponent<Animator>().SetBool("Menu", true);//FAde Out Creditos

        menu.GetComponent<Animator>().SetBool("Creditos", false);//Fade In Menu
    }

}
