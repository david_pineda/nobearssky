﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookAtBillboard : MonoBehaviour {

    public Camera m_Camera;
    public bool amActive = false;
    public bool autoInit = true;
    GameObject myContainer;

    void Awake()
    {
        if (autoInit == true)
        {
            m_Camera = Camera.main;
            amActive = true;
        }

        myContainer = new GameObject();
        myContainer.name = "GRP_" + transform.gameObject.name;
        myContainer.transform.position = transform.position;
        transform.parent = myContainer.transform;
    }

    void Start()
    {
        ChangeSprite();
    }

    void ChangeSprite()
    {
        if (amActive == true)
        {
            myContainer.transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.up,
            m_Camera.transform.rotation * Vector3.up);
        }
    }
}
