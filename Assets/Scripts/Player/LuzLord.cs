﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzLord : MonoBehaviour {

    Light luz;
    IndicadorDiaNoche indicador;

	// Use this for initialization
	void Start () {
        indicador = IndicadorDiaNoche.Instancia;

        luz = GetComponent<Light>();
        ApagarLuz();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (indicador.dia == true)
        {
            ApagarLuz();
        }
        if (indicador.dia == false)
        {
            PrenderLuz();
        }
	}

    void PrenderLuz()
    {
        luz.range = 100;
        luz.intensity = 2.1f;
    }

    void ApagarLuz()
    {
        luz.range = 1;
        luz.intensity = 1;
    }
}
