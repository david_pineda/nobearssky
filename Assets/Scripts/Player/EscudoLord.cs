﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscudoLord : MonoBehaviour {

    float multiplicador;
    Vector3 radioMax; 
    Vector3 radioOriginal;

    bool usando, creciendo; //booleanos que controlan cuando se esta usando el escudo y cuando esta creciendo o decreciendo

    bool recargado;
    float timerParaUsar;
    float speed;

    UILord ui;
    Color alpha;

    private AudioSource sonar;
    public AudioClip sonidoUp, sonidoDown;

	void Start () {
        sonar = GetComponent<AudioSource>();
        GetComponent<Renderer>().enabled = false; //Esto debe ocultar el objeto escudo

        ui = UILord.Instancia;
        alpha = ui.escudoText.color;
        alpha.a = 1;
        ui.escudoText.color = alpha;
        ui.escudoText.text = "Escudo Cargado";

        InvokeRepeating("RevisarTexto", 0, 1);

        MovimientoPersonaje.EnEscudo += UsarEscudo;
        UpgradeManager.EnMejoraEscudo += MejoraEscudo;

        multiplicador = 2.5f;
        radioOriginal = transform.localScale; //Donde se guarda la escala original.
        radioMax = transform.localScale * multiplicador; //Se controla estos dos parametros para subir de nivel el escudo
        usando = false;
        creciendo = false;

        recargado = true; //este se encarga de decir si se puede usar o no
        timerParaUsar = 10; //el timer que se encarga de "recargarlo"
        speed = 1; //la velocidad con la que se multiplica el tiempo de recarga

        Reinicio.EnReiniciando += Reiniciar;
	}
	
	void FixedUpdate ()
    {
		if (usando && creciendo) //Esto controla la forma en la que funciona el escudo
        {
            transform.localScale = Vector3.Lerp(transform.localScale, radioMax, Time.deltaTime * 10);

            if (!sonar.isPlaying)
            {
                sonar.clip = sonidoUp;
                sonar.Play();
            }
        }
        if (usando && !creciendo)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, radioOriginal, Time.deltaTime * 10);
            if (!sonar.isPlaying)
            {
                sonar.clip = sonidoDown;
                sonar.Play();
            }
            if (transform.localScale.x <= radioOriginal.x * 1.1f) //Se multiplica para que desaparezca antes 
            {
                usando = false;
                GetComponent<Renderer>().enabled = false;
                recargado = false;
            }
        }

        if (transform.localScale == radioMax) 
        {
            creciendo = false;
        }


        if (recargado == false) //Esto controla el tiempo de recarga
        {
            timerParaUsar -= Time.deltaTime * speed;

            if (timerParaUsar <= 0)
            {
                recargado = true;
                timerParaUsar = 10;

                alpha.a = 1;
                ui.escudoText.color = alpha;
                ui.escudoText.text = "Escudo Cargado";
            }
        }
	}

    void RevisarTexto()
    {
        if (ui.escudoText.color.a > 0)
        {
            alpha.a -= Time.deltaTime;
            ui.escudoText.color = alpha;
        }
    }

    void UsarEscudo()
    {
        if (usando == false && recargado == true)
        {
            usando = true;
            creciendo = true;
            GetComponent<Renderer>().enabled = true;

            alpha.a = 0;
            ui.escudoText.color = alpha;
            ui.escudoText.text = "";
        }
        else if(recargado == false)
        {
            //print("recargando... faltan " + timerParaUsar.ToString() + "  segundos");
            alpha.a = 1;
            ui.escudoText.color = alpha;
            ui.escudoText.text = "Recargando";
        }
    }


    void MejoraEscudo()
    {
        speed += 0.5f;
    }

    void OnTriggerStay (Collider other) //Aqui se revisa que los enemigos estén en el escudo y que esté activado
    {
        if (other.CompareTag("Enemy") && usando) //Los enemigos deben tener este tag
        {
            print("BAMBOOZLED");
            var script = other.GetComponent<EnemigoV2>();//.shocked();
            if (script!=null)
            {
                script.shocked();
            }
            else
            {
                 other.GetComponent<EnemigoV2Molesta>().shocked();
                print("enemigo error");
            }
        }
    }


    void Reiniciar()
    {
        speed = 1;
    }
}
