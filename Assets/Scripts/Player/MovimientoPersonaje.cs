﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class MovimientoPersonaje : MonoBehaviour {

    public float speed = 4f;

    Rigidbody rb;
    Vector3 movement;

    public bool parar;
    public bool raptando;
    //para controlar la energía
    //private bool moviendo; 
    private float energiaTotal;
    private float energiaActual;
    private float energiaAGastar;
    private float upgradeEnergia;

    private PausaLord pausa;
    private UILord ui;

    private AudioSource sonar;
    public AudioClip pausaSonido, movimientoSonido;

    private bool pausado;

    private Perdida ocurreAlgo;

    public delegate void TeclaPresionada();
    public static event TeclaPresionada EnInteraccion;
    public static event TeclaPresionada EnEscudo;

    public delegate void QuedarseSinEnergia();
    public static event QuedarseSinEnergia EnFaltaDeEnegia;


    private Animator anim;
    private Animator anim2;//ChocolatoSprite
    private GameObject manos;
    private float ping;

    // para el screen sake
    public Camera camarita;


    void Start()
    {
        ping = 0; //Debe hacer que la energía parpadee
        try
        {
            anim = GameObject.Find("Manos").GetComponent<Animator>(); //componente Animador
            anim2 = GameObject.Find("ChocolatoSprite").GetComponent<Animator>();
        }
        catch
        {
            Debug.Log("Uno de los Animators no se puede encontrar");
        }

        pausado = false; //usado para evitar pausar dentro del menu
        ocurreAlgo = Perdida.Instancia;

        try
        {
            rb = GetComponent<Rigidbody>();
            PrepareRigidbody();
        }
        catch
        {
            Debug.Log("Le falta un RigidBody al personaje");
        }

        //moviendo = false; //No se está moviendo

        pausa = PausaLord.Instancia; //Se busca el singleton pausaLord
        ui = UILord.Instancia; //Se busca el singleton UILord
        PrepareEnergia(); //Para ponerle valores a la enegía y al slider de energia

        sonar = GetComponent<AudioSource>();

        Recursos.EnParada += Detener;
        Recursos.EnSalida += Continuar;
        UpgradeManager.EnMejoraEnergia += EnergiaUpgrade;
        NaveTrigger.EnEntrada += RecargarEnergia;
        NaveTrigger.EnMenu += PausarSonidos;
        NaveTrigger.EnSalida += DejarDePausarSonidos;
        TimeWarpLord.EnPierdeUnDia += UsarEnergiaSolar;

        Recursos.EnRecogiendoRecurso += Taladrando;
        Recursos.EnTerminarDeRecoger += DejarDeTaladrar;
        Recursos.EnNoSePuede += TaladroFallido;

        ui.indicaciones.text = ""; //Esto es solo para cosas del escudo

        // esto es momentaneo 
        manos = GameObject.Find("Manos");
        manos.SetActive(false);  // sale un error y ni idea de why //El por qué es que se busca el animator de las manos para hacer funcionar el taladro
        //

        Reinicio.EnReiniciando += Reiniciar;
    }

    void PrepareRigidbody()
    {
        rb.useGravity = true;
        rb.isKinematic = false;
        rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
    }

    void PrepareEnergia()
    {
        energiaTotal = 100; //Cuanta energía total tiene
        energiaActual = energiaTotal; //Cuanta energía tiene actualmente
        energiaAGastar = 2.5f; //Cuanta energía gasta por frame
        upgradeEnergia = 0.5f; //Por cuanto va a aumentar la velocidad del robot

        ui.energia.maxValue = energiaTotal; //Se establece el maximo del slider
        ui.energia.value = energiaActual; //Se establece el valor actual del slider
    }


    void FixedUpdate()
    {

      


        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        if (Input.GetButtonDown("Fire1"))
        {
            EnEscudo();
        }

        Move(h, v);
        if ((h != 0 || v != 0) && energiaActual > 0 && movement != Vector3.zero)
        {
            GamePad.SetVibration(0, 0.1f, 0.1f);
            Energia();
            SonarMovimiento();
        }
        if (movement == Vector3.zero || parar == true) //para parar el audio
        {
            GamePad.SetVibration(0, 0, 0);
            PararSonido();
        }

        if (energiaActual <= 60 && energiaActual > 30)
        {
            ping = Mathf.PingPong(Time.time, 1);
            ParpadearEnergía();
        }

        if (energiaActual <= 30)
        {
            ping = 0;
            ParpadearEnergía();
            ui.indicaciones.color = new Color(1, 1, 1, 1);
        }
    }

    void LateUpdate() //De aqui salen la mayoría de inputs del jugador
    {
        if (Input.GetKeyUp(KeyCode.JoystickButton2) || Input.GetKeyUp(KeyCode.E))
        {
            EnInteraccion(); //Cuando se interactúa con algo. Se envia a Recursos y a Nave
        }
        if (Input.GetKeyUp(KeyCode.JoystickButton7) || Input.GetKeyUp(KeyCode.Escape))
        {
            if (!pausado)
            {
                SonarPausa();
                pausa.Pausar(); //Se llama el comando pausar de PausaLord
            }
        }
    }


    ///Acciones

    public void Detener()
    {
        parar = true;
        GamePad.SetVibration(0, 0, 0);
    }

    public void Continuar()
    {
        parar = false;
    }

    void Move(float h, float v)
    {

        // Set the movement vector based on the axis input.
        movement.Set(h, 0f, v);

        // Normalise the movement vector and make it proportional to the speed per second.
        movement = movement.normalized * speed * 100 * Time.deltaTime;


        if (parar == true)
        {
            movement = Vector3.zero;
        }

        // Move the player to it's current position plus the movement.
        rb.velocity = (movement);

    }

    void Energia() //Metodo para controlar la energía gastada
    {
        if (rb.velocity != Vector3.zero)
        {
            energiaActual -= energiaAGastar * Time.deltaTime;
            MostrarEnergia();

            if (energiaActual <= 0)
            {
                Detener();
                StartCoroutine(Wait(2));
            }
        }
    }


    IEnumerator Wait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (seconds == 2)
        {
            ocurreAlgo.accidente = true;
            EnFaltaDeEnegia(); //Se envía a TimewarpLord
        }
        else
        {
            Continuar();
        }
    }


    void UsarEnergiaSolar() //Solo le devuelve la energía al robot
    {
        StartCoroutine(Wait(2.1f));
        energiaActual += energiaTotal;
        RecargarEnergia();
        MostrarEnergia();
    }




    void EnergiaUpgrade() //Metodo que recibe el evento de upgrade de la energía
    {
        speed += upgradeEnergia; //Mejora de energía
        if (energiaAGastar >= 2f)
        {
            energiaAGastar -= 0.5f; //Disminuye la energía que se gasta
        }
        else
        {
            energiaAGastar /= 1.1f;
        }
    }

    void RecargarEnergia()
    {
        energiaActual += 1;
        NoParpadearEnergia();
        if (energiaActual > energiaTotal)
        {
            energiaActual = energiaTotal;
            MostrarEnergia();
        }
    }


    void MostrarEnergia()
    {
        ui.energia.value = energiaActual; //Se establece el valor actual del slider
    }

    void ParpadearEnergía() //Para que parapdeen las cosas
    {
        ui.fillEnergia.color = new Color(1, ping, 0);
        ui.indicaciones.text = "Regresar a la Nave";
        ui.indicaciones.color = new Color(1, 1, 1, ping);
    }
     
    void NoParpadearEnergia() //Lo contario de la anterior
    {
        ui.fillEnergia.color = ui.fillColor;
        ui.indicaciones.text = "";
        ui.indicaciones.color = new Color(1, 1, 1, 1);
    }

    public void raptado(float x, float z)
    {
        //this.transform.position(x, this.transform.position.y, z);
        this.transform.position = new Vector3(x, this.transform.position.y, z);
        anim2.SetBool("raptado", true);
    }
    public void desraptado() {
        anim2.SetBool("raptado", false);
    }

    ////Sonidos

    private void SonarMovimiento()
    {
        if (!sonar.isPlaying)
        {
            sonar.clip = movimientoSonido; //se cambia el sonido al de movimiento
            sonar.loop = true;
            sonar.volume = 0.16f;
            sonar.pitch = 3;

            sonar.Play(); //Hacer que suene el audio
        }
    }

    private void PararSonido()
    {
        if (sonar.isPlaying && sonar.clip == movimientoSonido)
        {
            sonar.Stop(); //Hacer que pare el audio
        }
    }

    private void SonarPausa()
    {
        if (!pausa.noPausar)
        {
            sonar.Pause();

            sonar.clip = pausaSonido; //se cambia el sonido al de pausa
            sonar.loop = false;
            sonar.volume = 0.5f;
            sonar.pitch = 1;

            sonar.Play();
        }
    }

    private void PausarSonidos()
    {
        sonar.Pause();
        pausado = true;
    }

    private void DejarDePausarSonidos()
    {
        sonar.UnPause();
        pausado = false;
    }


    ///Animaciones
    ///
    private void Taladrando()
    {
        // esto es momentaneo 
        manos.SetActive(true);
        //
        camarita.GetComponent<ComportamientoCamara>().Shakecamera(true);
        anim.SetBool("Taladrando", true);
        anim2.SetBool("Taladro", true);
    }

    private void DejarDeTaladrar()
    {

        camarita.GetComponent<ComportamientoCamara>().Shakecamera(false);
        anim.SetBool("Taladrando", false);
        anim2.SetBool("Taladro", false);

        // esto es momentaneo 
        //manos.SetActive(false);
        //
    }

    void TaladroFallido()
    {

    }

    void Reiniciar()
    {
        speed = 4f;
        this.gameObject.GetComponent<Transform>().position = new Vector3(0, 1.2f, 0);

        ping = 0; //Debe hacer que la energía parpadee
        pausado = false; //usado para evitar pausar dentro del menu

        PrepareEnergia(); //Para ponerle valores a la enegía y al slider de energia
        ui.indicaciones.text = ""; //Esto es solo para cosas del escudo

        manos.SetActive(false);
        DejarDeTaladrar();
    }
}
