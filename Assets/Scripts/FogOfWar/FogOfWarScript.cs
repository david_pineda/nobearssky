﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWarScript : MonoBehaviour {

    public Transform FogOfWarPlane;
    public int Number = 1;

    private float cambiante;

    public Vector4 playerPos;

    private IndicadorDiaNoche indicador; //cosas extra para que se vea mejor
    Color alpha;

    void Start()
    {
        //Poner en Player
        //El script tambien debe ir en la Nave (cambiando el Number a 2) y en el final (Number 3)

        FogOfWarPlane = GameObject.FindWithTag("Fog").GetComponent<Transform>();

        indicador = IndicadorDiaNoche.Instancia;
        alpha = Color.black;

        cambiante = 0f;
    }

    void Update()
    {
        /*
        Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position);
        Ray rayToPlayerPos = Camera.main.ScreenPointToRay(screenPos);
        int layermask = (int)(1 << 8);
        RaycastHit hit;
        if (Physics.Raycast(rayToPlayerPos, out hit, 1000, layermask))
        {
            print("is this real");
            FogOfWarPlane.GetComponent<Renderer>().material.SetVector("_Player" + Number.ToString() + "_Pos", hit.point);
        }
        */ //Codigo Original


        playerPos = new Vector4(transform.position.x, transform.position.y, transform.position.z);

        FogOfWarPlane.GetComponent<Renderer>().material.SetVector("_Player" + Number.ToString() + "_Pos", playerPos);

        if (Number == 1)
        {
            FogOfWarPlane.transform.position = playerPos;
        }
    }

    void LateUpdate()
    {
        if (indicador.dia == true)
        {
            if (alpha.a < 1)
            {
                alpha.a += Time.deltaTime * 0.1f;
            }
            FogOfWarPlane.GetComponent<Renderer>().material.color = alpha;
            FogOfWarPlane.GetComponent<Renderer>().material.SetFloat("FogMaxRadius", 0.7f);
        }
        if (indicador.dia == false)
        {
            alpha.a -= Time.deltaTime * 0.1f;
            if (alpha.a <= 0.1f)
            {
                alpha.a = 0.1f;
            }
            FogOfWarPlane.GetComponent<Renderer>().material.color = alpha;
            FogOfWarPlane.GetComponent<Renderer>().material.SetFloat("FogMaxRadius", 0.6f);
        }
    }

    ///Cómo funciona:
    ///Se creó un shader nuevo y un material nuevo que se le pusieron a un plano FogOfWar.
    ///Este shader permite crear un circulo en varios puntos de la pantalla, siguiendo diferentes "jugadores"
    ///Para que el circulo siga al jugador, originalmente, se lanzaba un ray desde la camara para encontrarlo
    ///Pero como esto no sirvió, creé un Vector4 a partir del transform del jugador.
    ///Además
    ///Para que el FogOfWar tape todo sin ponerlo por encima de todo, se creó una camara nueva y un layer nuevo
    ///el layer se le puso al plano de FogOfWar.
    ///La camara nueva solo renderiza el FoW, mientras la main renderiza todo el resto
    ///La nueva camara tiene que ser hija de main para que funciona bien, y debe tener un depth mayor

}
