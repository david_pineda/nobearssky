﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CivilizacionFinal : MonoBehaviour {

    public delegate void CivilizacionEncontrada();
    public static event CivilizacionEncontrada EnFinal;

    private UILord ui;

    float tiempoFinal;

    bool gameOver;

    void Start ()
    {
        ui = UILord.Instancia;
        tiempoFinal = 5;

        gameOver = false;
    }


    void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag("Player") && gameOver == false)
        {
            ui.sliderRecoleccion.gameObject.SetActive(true);
            ui.sliderRecoleccion.maxValue = tiempoFinal;
            ui.sliderRecoleccion.value = 0;
        }
    }

    void OnTriggerStay (Collider other)
    {
        if (other.CompareTag("Player") && gameOver == false)
        {
            ui.sliderRecoleccion.value += Time.deltaTime * 10;

            if (ui.sliderRecoleccion.value >= tiempoFinal)
            {
                ui.sliderRecoleccion.gameObject.SetActive(false);
                EnFinal();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && gameOver == false)
        {
            ui.sliderRecoleccion.gameObject.SetActive(false);
            ui.sliderRecoleccion.value = 0;
        }
    }

}
